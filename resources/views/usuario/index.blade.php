@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/esa/portal_administrador">Home</a></li>
                    <li class="breadcrumb-item active">Usuario</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">Gerenciamento de Usuario</div>
                    <div class="panel-body">
                        <!-- Trigger the modal with a button -->
                        <a type="button" class="btn btn-info col-md-3 " data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus-sign"></span> Usuario</a>
                        <!-- Modal -->
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Cadastro de Usuário</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal" method="POST" action="{{ route('usuario.save') }}">
                                            {{ csrf_field() }}

                                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                <label for="name" class="col-md-4 control-label">Usuario</label>

                                                <div class="col-md-6">
                                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                                    @if ($errors->has('name'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>


                                            <div class="form-group{{ $errors->has('matricula') ? ' has-error' : '' }}">
                                                <label for="matricula" class="col-md-4 control-label">Matricula</label>

                                                <div class="col-md-6">
                                                    <input id="matricula" type="text" class="form-control" name="matricula" value="{{ old('matricula') }}" required autofocus>

                                                    @if ($errors->has('matricula'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('matricula') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email" class="col-md-4 control-label">E-Mail</label>

                                                <div class="col-md-6">
                                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('tipo') ? ' has-error' : '' }}">
                                                <label for="tipo" class="col-md-4 control-label">Tipo Usuario</label>

                                                <div class="col-md-6">
                                                    <select name="tipo" class="form-control" id="tipo" placeholder="Selecione o tipo">
                                                        <option value="#">Selecione o tipo</option>
                                                        @foreach($tipoUsuario as $tipo)
                                                            <option value="{{ $tipo->id }}" > {{ $tipo->nome_tipo_usuario }}</option>
                                                        @endforeach
                                                    </select>

                                                    @if ($errors->has('tipo'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('tipo') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        Salvar <span class="glyphicon glyphicon-ok-sign"></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">

                                    </div>
                                </div>

                            </div>
                        </div>

                        <br>
                        <br>
                        <br>
                    @if($verificarUsuario == 0)<!--se o verificador voltar com 0 ele nao tem nada-->
                        <table class="table table-dark table-hover">
                            <thead>
                            <tr>
                                <th class="col-md-8">Nome</th>
                                <th class="col-md-4">-</th>
                            </tr>
                            </thead>
                            <tbody>
                            Sem dados Cadastrados
                            </tbody>
                        </table>
                    @elseif($verificarUsuario == 1)<!--se o verificador voltar com 1 ele tem dados-->
                        <table class="table table-dark table-hover">
                            <thead>
                            <tr>
                                <th class="col-md-8">Nome Tipo</th>
                                <th class="col-md-4">-</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usuarios as $tipos)
                                <tr>
                                    <td>
                                        {{$tipos->name}}
                                    </td>
                                    <td>
                                        <a class="btn btn-info" href="{{route('usuario.edit', $tipos->id)}}">Editar <span class="glyphicon glyphicon-pencil	Try it
"></span></a>
                                        <a class="btn btn-danger" href="{{route('usuario.remove', $tipos->id)}}">Excluir <span class="glyphicon glyphicon-trash"></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
