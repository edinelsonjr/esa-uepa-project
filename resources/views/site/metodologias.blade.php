<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Fisioterapia - UEPA</title>

    <!-- Bootstrap core CSS -->
    <link href="/bower_components/bootswatch-dist/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/bower_components/bootswatch-dist/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/bower_components/bootswatch-dist/https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="/bower_components/bootswatch-dist/https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="/bower_components/bootswatch-dist/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/bower_components/bootswatch-dist/css/freelancer.min.css" rel="stylesheet">

</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
    <div class="container">

        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/" style="font-family: sans-serif; font-size: 15px">Início</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/esa/docentes/" style="font-family: sans-serif; font-size: 15px">Docentes</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/esa/metodologias_ensino_aprendizagem/" style="font-family: sans-serif; font-size: 15px">METODOLOGIAS</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/esa/organizacao" style="font-family: sans-serif; font-size: 15px">Organização</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/home" style="font-family: sans-serif; font-size: 15px">AVALIAÇÃO CONTINUADA</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<br>
<br>
<br>

<!-- Portfolio Grid Section -->
<section id="portfolio" class="portfolio">
    <div class="container">
        <h5 class="text-center text-uppercase text-secondary mb-0">	Aprendizagem Baseada em Problemas (ABP) ou Problem-Based Learning (PBL):</h5>
        <br>
        <div class="row" style="font-family: sans-serif;">
            <p align="justify">
                O ABP, também conhecido como PBL será a estratégia de ensino a ser aplicada em todos os Módulos Temáticos (MT) deste curso, desde o primeiro semestre (1o período) até o sexto semestre (6o período) e eventualmente nos períodos seguintes, sendo, portanto, um dos principais recursos metodológicos a ser empregado para ensinar aos futuros fisioterapeutas sobre os mais relevantes conhecimentos e condutas de sua atuação profissional.
            </p>


            <ul class="list-group">
                <li align="justify">
                    Objetivo:
                    <ul>
                        <li align="justify">Estimula no estudante a capacidade de aprender a aprender, de trabalhar em equipe e de ouvir outras opiniões, mesmo que contrárias às suas.</li>
                    </ul>
                </li>
                <li align="justify">
                    Como funciona:
                    <ul>
                        <li align="justify">Nesta estratégia de ensino-aprendizagem os professores atuarão como tutores ou facilitadores em pequenos grupos de estudantes (grupo tutorial), tendo assim a oportunidade de acompanhar melhor a evolução dos acadêmicos, assim como também poderão atuar de forma mais específica e eficiente para alcançar o melhor rendimento de cada estudante. </li>
                        <li align="justify">Os acadêmicos serão divididos em grupos de até 10 membros e cada um destes grupos será acompanhado por um professor (facilitador/tutor), que será responsável por aplicar o ABP, que em síntese seguirá o seguinte: Em um dia específico de cada semana os estudantes se reúnem com o docente (tutor/facilitador) para um primeiro encontro (“abertura do problema”), o qual deve iniciar com a escolha, entre os acadêmicos, de um coordenador e um assistente (ou secretário).  </li>
                        <li align="justify">Todos os outros acadêmicos que não foram escolhidos como coordenador ou assistente/secretário do grupo devem participar ativamente, e respeitosamente, dos momentos de discussão contribuindo com seus conhecimentos e experiências (no encontro de “abertura do problema”), assim como com os novos conhecimentos adquiridos (no encontro de “fechamento do problema”), argumentando com as referências bibliográficas estudadas.  </li>
                        <li align="justify">Devem ainda ajudar o grupo a solucionar todas as questões e objetivos de aprendizado levantados, além de sumarizar suas anotações e socializá-las para facilitar o trabalho do coordenador e do assistente ou secretário. </li>
                        <li align="justify">Após a definição dos personagens essenciais para aplicação do ABP, segue-se com a apresentação, pelo professor (tutor/facilitador), de um texto previamente elaborado e que aborda uma situação-problema que versa sobre um ou alguns dos objetivos de aprendizado do módulo temático em questão. </li>
                        <li align="justify">Com o texto nas mãos, os estudantes são então solicitados a realizarem a sua leitura detalhada, que só será considerada concluída quando todos os termos e expressões utilizados forem devidamente esclarecidos. Após a leitura inicial e compreensão do texto, o facilitador juntamente com o coordenador deverão estimular os estudantes a identificarem os pontos-chave do texto.  </li>
                        <li align="justify">O assistente por sua vez, deverá anotar tais pontos no quadro. Com os pontos-chave abordados pelo texto devidamente identificados, novamente o facilitador com ajuda do coordenador deverão estimular os acadêmicos a iniciarem um debate sobre cada um destes pontos, de forma ordeira e respeitosa, a partir de seus conhecimentos prévios (mesmo que empíricos e/ou errôneos). Apesar da contribuição inicial do facilitador, o coordenador e o assistente devem ser os personagens principais a se empenharem para manter o debate em bom nível.   </li>
                        <li align="justify">Além disso, estes dois personagens, juntamente com os demais membros do grupo, devem realizar um resumo destas ideias para que sejam expostas no quadro pelo assistente.    </li>
                        <li align="justify">Este primeiro encontro será concluído com o professor conduzindo os estudantes a formularem perguntas relacionadas ao problema debatido e que representem aquilo que os estudantes consideraram importante para aprender a partir da situação problema. </li>
                        <li align="justify">Esta etapa do processo deve ser realizada no tempo livre que os estudantes tiverem entre o encontro inicial (“abertura do problema”) e o próximo (“fechamento do problema”). </li>
                        <li align="justify">Esta etapa do processo deve ser realizada no tempo livre que os estudantes tiverem entre o encontro inicial (“abertura do problema”) e o próximo (“fechamento do problema”). </li>
                        <li align="justify">O coordenador novamente terá o papel de garantir a palavra a todos os estudantes, assim como estimular e mediar este novo debate a partir das diversas informações trazidas pelos estudantes. </li>
                        <li align="justify">Vale ressaltar que neste momento do processo, o facilitador não deverá ministrar uma aula sobre este(s) tema(s), e sim atuar em conjunto com o coordenador para estimular os estudantes a discutirem sobre a situação problema apresentada, e eventualmente corrigir equívocos de entendimento por parte dos acadêmicos. </li>
                        <li align="justify">Este momento do processo só será concluído quando todos os objetivos de aprendizagem ou perguntas produzidas no primeiro encontro (“abertura do problema”) tiverem sido respondidos de forma satisfatória pelos acadêmicos, e que tenham se esgotado todas as contribuições trazidas pelos acadêmicos, a partir de suas pesquisas. </li>
                        <li align="justify">Por fim segue-se a etapa de avaliação deste processo, que se processará de forma “formativa” e “somativa”. Na avaliação “formativa”, cada estudante terá cerca de 3 minutos para expressar seu ponto de vista sobre sua atuação dentro deste processo, assim como a atuação de seus demais colegas (coordenador e assistente também serão avaliados quanto ao desempenho de suas funções específicas) e do facilitador. </li>
                        <li align="justify">O facilitador também terá este mesmo tempo para realizar seus comentários sobre estes mesmos aspectos. A avaliação “somativa” se dará por intermédio da aplicação de um instrumento avaliativo, previamente elaborado, no qual os acadêmicos atribuirão notas para o seu desempenho, assim como de seus colegas, neste processo.</li>
                        <li align="justify">De forma mais sintética, pode-se apresentar esta metodologia com a descrição de seus 8 Passos (adaptado de BERBEL, 1998): 1- Ler atentamente o problema e esclarecer os termos desconhecidos; 2- Identificar as questões (problemas) propostas pelo enunciado; 3- Oferecer explicações para estas questões com base no conhecimento prévio que o grupo tem sobre o assunto; 4- Resumir estas explicações; 5- Estabelecer objetivos de aprendizagem que levem o aluno ao aprofundamento e complementação destas explicações; 6- Estudo individual respeitando os objetivos alcançados; 7- Rediscussão no grupo tutorial dos avanços do conhecimento obtidos pelo grupo; 8- Avaliação Formativa.</li>
                    </ul>
                </li>
            </ul>
            <br>

        </div>
    </div>
    <br>
    <div class="container">
        <h5 class="text-center text-uppercase text-secondary mb-0">	Metodologia de Ensino pela Problematização:</h5>
        
        <div class="row" style="font-family: sans-serif;">
            <ul class="list-group">
                <li align="justify">
                    Definição:
                    <ul>
                        <li align="justify">Também conhecido como Ensino Baseado na Investigação, a metodologia de ensino pela Problematização será aplicada neste currículo principalmente dentro do eixo “Atividade Integrada”, em virtude dos conteúdos apresentarem características que favoreçam a aplicação desta metodologia (visitas de pequenos grupos de acadêmicos, acompanhados de um docente, a cenários de prática), bem como buscam enfatizar a percepção dos estudantes sobre a realidade vivenciada por eles, para o seu aprendizado, apresentando, portanto, plenas condições para potencializar o processo ensino-aprendizagem com esta metodologia. </li>
                    </ul>
                </li>
                <li align="justify">
                    Referência:
                    <ul>
                        <li align="justify">A metodologia de ensino pela Problematização fundamenta-se na pedagogia libertadora de Paulo Freire, nos princípios do materialismo histórico dialético e no construtivismo de Piaget, nos quais prima-se pelo desenvolvimento da capacidade do estudante em participar como agente de transformação social, durante o processo de detecção de problemas reais e de busca por soluções originais, estando portanto também em consonância com proposta priorizada neste projeto político pedagógico (PPP) de formação de um profissional/cidadão crítico e transformador de sua realidade (FREITAS, 2010; SILVA, 2006).</li>
                        <li align="justify">Esta também não é uma metodologia centrada no professor, necessitando uma mudança radical em relação a uma postura tradicional. Para Pereira (2003, p.153), na metodologia problematizadora “o professor está no mesmo patamar de importância em relação aos alunos, visto que seu papel é estimular a discussão”. Percebe-se, portanto, que nesta metodologia de ensino o professor realiza um papel de “provador” ou “instigador” e que o método de ensino baseia-se na relação dialógica entre discentes e docentes. </li>
                        <li align="justify">De acordo com Berbel (2012), as primeiras referências acerca da Metodologia da Problematização foram divulgadas no livro “Estratégias de Ensino-aprendizagem”, de Bordenave e Pereira (1977), no qual é apresentado o chamado Arco de Maguerez, que é constituído pelos seguintes movimentos: observação da realidade, pontos-chave, teorização, hipóteses de solução e aplicação à realidade.</li>
                    </ul>
                </li>
                <br>
                <br>
            </ul>

            <div class="container">
                <h5 class="text-center text-uppercase text-secondary mb-0">RELAÇÃO DOS COMPONENTES CURRICULARES</h5>
                <br>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="text-center"><img src="/img/metodologias/img.jpg" width="600px" height="450px" id="logo-header"></p>
                    </div>
                </div>
            </div>

            <ul class="list-group">
                <li align="justify">
                    Como funciona:
                    <ul>
                        <li align="justify">Inicialmente os acadêmicos serão divididos em subgrupos de, sendo que cada um destes grupos será coordenado por um docente, que se encarregará de acompanhá-los em um cenário de prática (a universidade, a comunidade, unidades de saúde, hospitais, clínicas de fisioterapia e outros locais relacionados com a atuação profissional do Fisioterapeuta e demais profissionais de saúde). </li>
                        <li align="justify">Em um dia específico estes acadêmicos (em seus respectivos grupos) deverão comparecer a um destes locais de prática, conforme indicado pelo professor. Neste cenário de prática o professor direcionará o grupo de acadêmicos para a observação de um aspecto específico (previamente planejado) da operacionalização daquele local de prática. </li>
                        <li align="justify">Uma vez direcionada pelo docente, a situação de interesse para a atividade, passará a ser acompanhada pelos discentes que assim ingressarão na primeira etapa, propriamente dita, da Metodologia da Problematização. Trata-se da etapa de “observação da realidade”, quando então os acadêmicos observarão atentamente aquela realidade e desenvolverão suas percepções pessoais, efetuando, assim, uma primeira leitura sincrética da realidade. Neste momento o professor poderá direcionar a análise dos estudantes, confrontando-os com perguntas que ajudem a focalizar o problema (jamais deve revelar o(s) problema(s) de forma explícita). </li>
                        <li align="justify">Após este momento de debate reflexivo, os estudantes devem então selecionar os pontos mais relevantes desta reflexão em grupo, enumerando os aspectos essenciais que devem ser abordados para a compreensão do problema. Cabe esclarecer que neste momento, poderá ser eleito apenas um desses problemas para todo o grupo estudar ou, então, vários deles, distribuídos um para cada pequeno sub-grupo. </li>
                        <li align="justify">A terceira etapa desta metodologia já poderá ser cumprida longe daquele cenário alvo da observação, em espaços aonde o acadêmico possa realizar uma busca por informações. Trata-se da etapa de “teorização do problema” ou investigação propriamente dita, que segundo Mitre et al. (2008) representa o momento em que os estudantes procederão por meio de pesquisa em fontes literárias confiáveis ou até mesmo entrevistas com especialistas, buscando a contribuição da ciência para esclarecimento dos “pontos-chave” identificados naquela realidade. </li>
                        <li align="justify">Depois de realizada esta etapa de pesquisa o docente e o grupo de discentes deverão novamente se encontrar em um novo momento, para assim concluírem a etapa de “teorização do problema”. Este encontro não precisa ser no local de prática e de observação da realidade.</li>
                        <li align="justify">Neste momento deverá ocorrer a socialização dos novos conhecimentos adquiridos e um novo debate para reflexão crítica e identificação das informações úteis para a solução dos problemas identificados, Vale ressaltar que as informações pesquisadas precisam ser analisadas e avaliadas, quanto à sua relevância para a resolução do problema, e nesse momento, o professor novamente apresentará um papel importante, podendo ser consultado pelos estudantes. </li>
                        <li align="justify">Com a realização adequada da etapa da “teorização do problema” espera-se que o acadêmico alcance a total compreensão do problema, ou seja, uma compreensão dos aspectos práticos ou situacionais e dos princípios teóricos que o permeiam, através da confrontação da realidade com sua teorização. </li>
                        <li align="justify">Deve-se ainda verificar se as hipóteses são aplicáveis à realidade e se o grupo poderá de fato executá-las (MITRE et al, 2008). Dependendo da complexidade do(s) problema(s) selecionado(s) pelo grupo, este momento de debate para a seleção das estratégias mais viáveis para a sua solução poderá ser repetido, conforme a decisão do professor em conjunto com o grupo de acadêmicos. Sugere-se que esta fase seja considerada concluída apenas quando professores e acadêmicos tiverem estabelecido uma estratégia viável para a solução do(s) problema(s) levantado(s) no cenário de prática. </li>
                        <li align="justify">Na última fase, a “aplicação à realidade”, os estudantes então retornam ao cenário de observação para executarem as estratégias escolhidas como sendo as mais viáveis para a solução do(s) problema(s) previamente identificado(s) pelo grupo. Haverá, assim, conforme Cyrino e Toralles-Pereira (2004), a aplicação das hipóteses identificadas para a solução do problema, ocorrendo uma interação entre estudante e objeto de estudo, com o intuito de estabelecer um diálogo transformador para ambos. </li>
                        <li align="justify">Ao completarem o Arco de Maguerez, os estudantes exercitam a dialética de ação-reflexão-ação, tendo sempre como ponto de partida a realidade social e assim podem aprender a generalizar o que foi vivenciado para utilizá-lo em diferentes situações. Vale ressaltar que após o estudo de um problema, podem surgir novos desdobramentos, exigindo a interdisciplinaridade para sua solução, o desenvolvimento do pensamento crítico e a responsabilidade do estudante pela própria aprendizagem.</li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <br>

</section>







<div class="copyright py-4 text-center text-white">
    <div class="container">
        <small>Copyright &copy; Todos os Direitos Reservados a Milene Duarte 2018</small>
    </div>
</div>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>


<!-- Bootstrap core JavaScript -->
<script src="/bower_components/bootswatch-dist/vendor/jquery/jquery.min.js"></script>
<script src="/bower_components/bootswatch-dist/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="/bower_components/bootswatch-dist/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="/bower_components/bootswatch-dist/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="/bower_components/bootswatch-dist/js/jqBootstrapValidation.js"></script>
<script src="/bower_components/bootswatch-dist/js/contact_me.js"></script>

<!-- Custom scripts for this template -->
<script src="/bower_components/bootswatch-dist/js/freelancer.min.js"></script>

</body>

</html>
