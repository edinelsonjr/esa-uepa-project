<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Fisioterapia - UEPA</title>

    <!-- Bootstrap core CSS -->
    <link href="/bower_components/bootswatch-dist/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/bower_components/bootswatch-dist/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/bower_components/bootswatch-dist/https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="/bower_components/bootswatch-dist/https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="/bower_components/bootswatch-dist/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/bower_components/bootswatch-dist/css/freelancer.min.css" rel="stylesheet">

</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
    <div class="container">

        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/" style="font-family: sans-serif; font-size: 15px">Início</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/esa/docentes/" style="font-family: sans-serif; font-size: 15px">Docentes</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/esa/metodologias_ensino_aprendizagem/" style="font-family: sans-serif; font-size: 15px">METODOLOGIAS</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/esa/organizacao" style="font-family: sans-serif; font-size: 15px">Organização</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/home" style="font-family: sans-serif; font-size: 15px">AVALIAÇÃO CONTINUADA</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<br>
<br>
<br>

<!-- Portfolio Grid Section -->
<section id="portfolio" class="portfolio">
    <div class="container" style="font-family: sans-serif;">
        <h5 class="text-center text-uppercase text-secondary mb-0">ORGANIZAÇÃO CURRICULAR</h5>
        <br>
        <div class="row">
            <p align="justify">De acordo com o novo Projeto Político Pedagógico implantado no ano de 2016, o curso de Fisioterapia da UEPA deve ser desenvolvido em 05 (cinco) anos, dispondo de uma concepção de currículo inovador e integrado orientado por competências e composto por unidades curriculares que buscam o saber interdisciplinar.</p>
            <p align="justify">Para os 3 primeiros anos do curso, a organização curricular abrange eixos verticais e horizontais. Os eixos verticais são componentes educacionais que podem durar de algumas semanas a um semestre inteiro, porém não mais que isso, de acordo com a densidade e complexidade dos temas abordados. Eles integram vários conteúdos. Os horizontais, por sua vez, ultrapassam os limites entre os semestres e interligam unidades de formação em um nível de complexidade crescente e complementar, em formato espiral, trabalhando no desenvolvimento de diversos conhecimentos e habilidades inerentes ao profissional fisioterapeuta e à inserção progressiva dos acadêmicos nos espaços de saúde desde o 1º período.</p>

            <div class="col-md-12 align-center">
                <h5 class="text-center text-uppercase text-secondary mb-0">COMPOSIÇÃO CURRICULAR</h5>
                <br>
                <img class="img-fluid" src="/img/organizacao/img.jpg" alt="">
            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <h5 class="text-center text-uppercase text-secondary mb-0">DISTRIBUIÇÃO DE CARGA HORÁRIA </h5>
        <br>
        <div class="row">
            <div class="col-md-12 align-center">
                <p class="text-center"><img src="/img/organizacao/img2.jpg" width="700px" height="450px" id="logo-header"></p>
            </div>
        </div>
    </div>
    <br>
    <div class="container">

        <div class="row">
            <div class="col-xl-12">
                <p class="text-center"><img src="/img/organizacao/img3.jpg" width="700px" height="450px" id="logo-header"></p>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <p class="text-center"><img src="/img/organizacao/img4.jpg" width="700px" height="450px" id="logo-header"></p>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <p class="text-center"><img src="/img/organizacao/img5.jpg" width="700px" height="450px" id="logo-header"></p>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <p class="text-center"><img src="/img/organizacao/img6.jpg" width="700px" height="450px" id="logo-header"></p>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <h5 class="text-center text-uppercase text-secondary mb-0">RELAÇÃO DOS COMPONENTES CURRICULARES</h5>
        <br>
        <div class="row">
            <div class="col-xl-12">
                <p class="text-center"><img src="/img/organizacao/img7.jpg" width="700px" height="1000px" id="logo-header"></p>
            </div>
        </div>
    </div>
</section>


<div class="copyright py-4 text-center text-white">
    <div class="container">
        <small>Copyright &copy; Todos os Direitos Reservados a Milene Duarte 2018</small>
    </div>
</div>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>

<!-- Portfolio Modals -->

<!-- Bootstrap core JavaScript -->
<script src="/bower_components/bootswatch-dist/vendor/jquery/jquery.min.js"></script>
<script src="/bower_components/bootswatch-dist/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="/bower_components/bootswatch-dist/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="/bower_components/bootswatch-dist/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="/bower_components/bootswatch-dist/js/jqBootstrapValidation.js"></script>
<script src="/bower_components/bootswatch-dist/js/contact_me.js"></script>

<!-- Custom scripts for this template -->
<script src="/bower_components/bootswatch-dist/js/freelancer.min.js"></script>

</body>

</html>
