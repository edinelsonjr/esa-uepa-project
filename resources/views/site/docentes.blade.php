<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Fisioterapia - UEPA</title>

    <!-- Bootstrap core CSS -->
    <link href="/bower_components/bootswatch-dist/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/bower_components/bootswatch-dist/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/bower_components/bootswatch-dist/https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="/bower_components/bootswatch-dist/https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="/bower_components/bootswatch-dist/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/bower_components/bootswatch-dist/css/freelancer.min.css" rel="stylesheet">

</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
    <div class="container">

        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/" style="font-family: sans-serif; font-size: 15px">Início</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/esa/docentes/" style="font-family: sans-serif; font-size: 15px">Docentes</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/esa/metodologias_ensino_aprendizagem/" style="font-family: sans-serif; font-size: 15px">METODOLOGIAS</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/esa/organizacao" style="font-family: sans-serif; font-size: 15px">Organização</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/home" style="font-family: sans-serif; font-size: 15px">AVALIAÇÃO CONTINUADA</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<br>
<br>
<br>

<!-- Portfolio Grid Section -->
<section id="portfolio" class="portfolio">
    <div class="container">
        <h5 class="text-center text-uppercase text-secondary mb-0">Corpo Docente</h5>
        <br>
        <div class="row" style="font-family: sans-serif;">
            <ul class="list-group">

                <li>Adjanny Estela</li>
                <li>Alexandre Magalhães</li>
                <li>Alexandre Oliveira</li>
                <li>Átila Barros</li>
                <li>Daliane Marinho</li>
                <li>Elidiane Kono</li>
                <li>Izabel Alcina</li>
                <li>John Vale</li>
                <li>Luis Fernando Gouvêa</li>
                <li>Mariana Furtado</li>
                <li>Milene Duarte</li>
                <li>Pedro Odimar dos Santos</li>
                <li>Richelma Barbosa</li>
                <li>Renata Portela</li>
                <li>Rodrigo Ferreira</li>
                <li>Rosineide Bentes</li>
                <li>Silvania Takanashi</li>
                <li>Suelen Ricarte</li>
                <li>Tiago Silveira</li>
                <li>Wanderson Almeida</li>
            </ul>

        </div>
    </div>
    <br>
    <br>

</section>






<div class="copyright py-4 text-center text-white">
    <div class="container">
        <small>Copyright &copy; Todos os Direitos Reservados a Milene Duarte 2018</small>
    </div>
</div>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>


<!-- Bootstrap core JavaScript -->
<script src="/bower_components/bootswatch-dist/vendor/jquery/jquery.min.js"></script>
<script src="/bower_components/bootswatch-dist/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="/bower_components/bootswatch-dist/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="/bower_components/bootswatch-dist/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="/bower_components/bootswatch-dist/js/jqBootstrapValidation.js"></script>
<script src="/bower_components/bootswatch-dist/js/contact_me.js"></script>

<!-- Custom scripts for this template -->
<script src="/bower_components/bootswatch-dist/js/freelancer.min.js"></script>

</body>

</html>
