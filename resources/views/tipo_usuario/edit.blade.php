@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/esa/portal_administrador">Home</a></li>
                    <li class="breadcrumb-item"><a href="/esa/tipo_usuario/index">Tipo Usuario</a></li>
                    <li class="breadcrumb-item active">Editar</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Tipo Usuario</div>

                    <div class="panel-body">
                        @foreach($buscado as $busca)
                            <form action="{{route('tipo_usuario.save_edit', $busca->id)}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="put">
                                <div class="form-group{{ $errors->has('nome_tipo_usuario') ? ' has-error' : '' }}">
                                    <label for="nome_tipo_usuario" class="col-md-4 control-label">Nome Tipo</label>
                                    <div class="col-md-6">
                                        <input id="nome_tipo_usuario" type="text" class="form-control" name="nome_tipo_usuario" value="{{$busca->nome_tipo_usuario}}">
                                        @if ($errors->has('nome_tipo_usuario'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('nome_tipo_usuario') }}</strong>
                                        </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Salvar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
