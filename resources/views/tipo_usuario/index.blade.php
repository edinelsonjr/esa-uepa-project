@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/esa/portal_administrador">Home</a></li>
                    <li class="breadcrumb-item active">Tipo Usuario</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">Gerenciamento de Tipos de Usuario</div>
                    <div class="panel-body">
                        <!-- Trigger the modal with a button -->
                        <a type="button" class="btn btn-info col-md-3 " data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus-sign"></span> Tipo Usuario</a>
                        <!-- Modal -->
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Cadastro de Tipo de Usuário</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal" method="POST" action="{{ route('tipoUsuario.save') }}">
                                            {{ csrf_field() }}
                                            <div class="form-group{{ $errors->has('nome_tipo_usuario') ? ' has-error' : '' }}">
                                                <label for="nome_tipo_usuario" class="col-md-4 control-label">Tipo Usuario</label>

                                                <div class="col-md-6">
                                                    <input id="nome_tipo_usuario" type="text" class="form-control" name="nome_tipo_usuario" value="{{ old('nome_tipo_usuario') }}" required autofocus>

                                                    @if ($errors->has('nome_tipo_usuario'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('nome_tipo_usuario') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        Cadastrar
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">

                                    </div>
                                </div>

                            </div>
                        </div>

                        <br>
                        <br>
                        <br>
                    @if($verificarTipoUsuario == 0)<!--se o verificador voltar com 0 ele nao tem nada-->
                        <table class="table table-dark table-hover">
                            <thead>
                            <tr>
                                <th class="col-md-8">Nome Tipo</th>
                                <th class="col-md-4">-</th>
                            </tr>
                            </thead>
                            <tbody>
                            Sem dados Cadastrados
                            </tbody>
                        </table>
                    @elseif($verificarTipoUsuario == 1)<!--se o verificador voltar com 1 ele tem dados-->
                        <table class="table table-dark table-hover">
                            <thead>
                            <tr>
                                <th class="col-md-8">Nome Tipo</th>
                                <th class="col-md-4">-</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tipoUsuarios as $tipos)
                                <tr>
                                    <td>
                                        {{$tipos->nome_tipo_usuario}}
                                    </td>
                                    <td>
                                        <a class="btn btn-info" href="{{route('tipoUsuario.edit', $tipos->id)}}">Editar <span class="glyphicon glyphicon-pencil	Try it
"></span></a>
                                        <a class="btn btn-danger" href="{{route('tipoUsuario.remove', $tipos->id)}}">Excluir <span class="glyphicon glyphicon-trash"></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
