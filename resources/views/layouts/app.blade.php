<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Fisioterapia-UEPA</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="/bower_components/bootswatch-dist/css/bootstrap-datepicker.css" rel="stylesheet"/>
    <script src="/bower_components/bootswatch-dist/js/bootstrap-datepicker.min.js"></script>
    <script src="/bower_components/bootswatch-dist/js/bootstrap-datepicker.pt-BR.min.js" charset="UTF-8"></script>
    <link href="/bower_components/bootswatch-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/bower_components/bootswatch-dist/css/style.css" rel="stylesheet">
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->

            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/') }}">INICIO</a></li>
                        <li><a href="{{ url('/login') }}">ACESSAR</a></li>
                        <li><a href="{{ url('/esa/usuario/cadastro_teste/') }}">CADASTRAR</a></li>
                    @else
                        <li><a href="{{ url('/') }}">INICIO</a></li>
                        <li class="dropdown">
                            </a><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>SAIR</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @if(Session::has('flash_message'))
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div align="center" class="alert {{Session::get('flash_message')['class'] }}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close"><span
                                    class="glyphicon glyphicon-remove"></span> </a>
                        {{Session::get('flash_message')['msg'] }}
                    </div>
                </div>
            </div>
        </div>
    @endif

    @yield('content')

    <!-- JavaScripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript">
        $('#exemplo').datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR",
            startDate: '+0d',
        });
    </script>

</body>
</html>
