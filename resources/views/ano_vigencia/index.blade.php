@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/esa/portal_administrador">Home</a></li>
                    <li class="breadcrumb-item active">Ano vigência</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">Gerenciamento de Ano Vigência</div>
                    <div class="panel-body">
                        <!-- Trigger the modal with a button -->
                        <a type="button" class="btn btn-info col-md-3 " data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus-sign"></span> Ano Vigência</a>
                        <!-- Modal -->
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Cadastro de Tipo de Usuário</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal" method="POST" action="{{ route('ano_vigencia.save') }}">
                                            {{ csrf_field() }}
                                            <div class="form-group{{ $errors->has('ano_turma') ? ' has-error' : '' }}">
                                                <label for="ano_turma" class="col-md-4 control-label">Ano Turma</label>
                                                <div class="col-md-6">
                                                    <input id="ano_turma" type="text" class="form-control" name="ano_turma" value="{{ old('ano_turma') }}" required autofocus>
                                                    @if ($errors->has('ano_turma'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('ano_turma') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('vigencia') ? ' has-error' : '' }}">
                                                <label for="vigencia" class="col-md-4 control-label">Vigência Avaliação</label>

                                                <div class="col-md-6">
                                                    <input id="vigencia" type="text" class="form-control" name="vigencia" value="{{ old('vigencia') }}" required autofocus>

                                                    @if ($errors->has('vigencia'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('vigencia') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        Cadastrar
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">

                                    </div>
                                </div>

                            </div>
                        </div>

                        <br>
                        <br>
                        <br>


                    @if($verificarAnosVigencias == 0)<!--se o verificador voltar com 0 ele nao tem nada-->
                        <table class="table table-dark table-hover">
                            <thead>

                            </thead>
                            <tbody>
                            Sem dados Cadastrados
                            </tbody>
                        </table>
                    @elseif($verificarAnosVigencias == 1)<!--se o verificador voltar com 1 ele tem dados-->
                        <table class="table table-dark table-hover">
                            <thead>
                            <tr>
                                <th class="col-md-1">Ano</th>
                                <th class="col-md-1">Vigência</th>
                                <th class="col-md-6">-</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($anosVigencias as $tipos)
                                <tr>
                                    <td>
                                        {{$tipos->ano}}
                                    </td>
                                    <td>
                                        {{$tipos->vigencia}}
                                    </td>
                                    <td>
                                        <a class="btn btn-info" href="{{route('ano_vigencia.detail', $tipos->id)}}">Detalhe <span class="glyphicon glyphicon-eye-open	Try it
"></span></a>
                                        <a class="btn btn-info" href="{{route('tipoUsuario.edit', $tipos->id)}}">Editar <span class="glyphicon glyphicon-pencil	Try it"></span></a>

                                        <a class="btn btn-danger" href="{{route('tipoUsuario.remove', $tipos->id)}}">Excluir <span class="glyphicon glyphicon-trash"></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
    $('#exemplo').datepicker({
        format: "dd/mm/yyyy",
        language: "pt-BR",
        startDate: '+0d',
    });
</script>


