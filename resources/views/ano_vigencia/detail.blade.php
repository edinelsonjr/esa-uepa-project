@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/esa/portal_administrador">Home</a></li>
                    <li class="breadcrumb-item"><a href="/esa/ano_vigencia/index/">Ano vigência</a></li>
                    <li class="breadcrumb-item active">Detalhe</li>
                </ol>
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="col-xs-12">
                            <div class="invoice-title">
                                <h2>Ano Vigência: #{{$anosVigencias->id}}</h2>

                                @if($anosVigencias->status_ano_vigencia == 1)
                                    <span class="pull-right">
                                        <a class="btn btn-danger" href="{{route('ano_vigencia.desativar', $anosVigencias->id)}}" type="button" data-toggle="tooltip" data-original-title="Desativar Ano Vigência">Desativar Ano Vigência <i class="glyphicon glyphicon-trash"></i></a>
                                    </span>
                                @elseif($anosVigencias->status_ano_vigencia == 0)
                                    <span class="pull-right">
                                        <a class="btn btn-success" href="{{route('ano_vigencia.ativar', $anosVigencias->id)}}" type="button" data-toggle="tooltip" data-original-title="Desativar Ano Vigência">Ativar Ano Vigência <i class="glyphicon glyphicon-ok"></i></a>
                                    </span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <address>
                                        <strong>Ano da Turma: </strong>{{$anosVigencias->ano}}<br>
                                        <strong>Vigência da Avaliação: </strong>{{$anosVigencias->vigencia}}<br>
                                        @if($anosVigencias->status_ano_vigencia == 1)
                                            <strong>Data Fim da Avaliação: </strong> <span class="badge badge-primary">Ativado</span> <br>
                                        @elseif($anosVigencias->status_ano_vigencia == 0)
                                            <strong>Data Fim da Avaliação: </strong> <span class="text-danger">Desativado</span><br>
                                    @endif


                                    </address>
                                </div>
                            </div>
                            <a class="btn btn-success col-md-3 col-md-offset-9" href="{{route('ano_vigencia.vincularUsuario', $anosVigencias->id)}}">Vincular Usuario <span class="glyphicon glyphicon-plus-sign"></span></a>
                        </div>
                    </div>
                    <div class="panel-heading">
                        <strong class="col-md-offset-0">Usuários Vinculados ao Ano Vigência</strong>
                    </div>
                    <div class="panel-body">

                    @if($verificarUsuarios == 0)<!--se o verificador voltar com 0 ele nao tem nada-->
                        <table class="table table-dark table-hover">
                            <thead>
                            <tr>
                                <th class="col-md-8">Nome</th>
                                <th class="col-md-4">-</th>
                            </tr>
                            </thead>
                            <tbody>
                            Sem dados Cadastrados
                            </tbody>
                        </table>
                    @elseif($verificarUsuarios == 1)<!--se o verificador voltar com 1 ele tem dados-->
                        <table class="table table-dark table-hover">
                            <thead>
                            <tr>
                                <th class="col-md-8">Nome</th>
                                <th class="col-md-4">-</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usuarioAnoVigecia as $tipos)
                                <tr>
                                    <td>
                                        {{$tipos->name}}
                                    </td>
                                    <td>
                                        <a class="btn btn-danger col-md-12" href="{{route('ano_vigencia.desvincularUsuario', $tipos->usuarioAnoVigenciaId)}}">Desvincular <span class="glyphicon glyphicon-trash"></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
            </div>
        </div>
    </div>
@endsection
