@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/esa/portal_administrador">Home</a></li>
                    <li class="breadcrumb-item"><a href="/esa/ano_vigencia/index/">Ano vigência</a></li>
                    <li class="breadcrumb-item"><a href="{{route('ano_vigencia.detail', $idUsuarioAnoVigencia)}}">Detalhe</a></li>
                    <li class="breadcrumb-item active">Vincular Usuario</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">Vincular Usuario</div>
                    <div class="panel-body">
                        <!-- Trigger the modal with a button -->
                        <form class="form-horizontal" method="POST" action="{{ route('ano_vigencia.vincularUsuarioSave') }}">
                            {{ csrf_field() }}

                            <input type="hidden" name="idAnoVigencia" value="{{$idUsuarioAnoVigencia}}">
                            <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
                                <label for="nome" class="col-md-4 control-label">Usuário</label>

                                <div class="col-md-6">
                                    <select name="usuario" class="form-control" id="usuario" placeholder="Selecione o tipo">
                                        <option value="#">Selecione Usuario</option>
                                        @foreach($users as $tipo)
                                            <option value="{{ $tipo->idUsers }}" > {{ $tipo->nameUsers }}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('usuario'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('usuario') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Salvar <span class="glyphicon glyphicon-ok-sign"></span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
