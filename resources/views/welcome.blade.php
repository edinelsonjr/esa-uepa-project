<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ESA - UEPA</title>

    <!-- Bootstrap core CSS -->
    <link href="/bower_components/bootswatch-dist/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/bower_components/bootswatch-dist/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/bower_components/bootswatch-dist/https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="/bower_components/bootswatch-dist/https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="/bower_components/bootswatch-dist/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/bower_components/bootswatch-dist/css/freelancer.min.css" rel="stylesheet">

</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
    <div class="container">

        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/" style="font-family: sans-serif; font-size: 15px">Início</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/esa/docentes/" style="font-family: sans-serif; font-size: 15px">Docentes</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/esa/metodologias_ensino_aprendizagem/" style="font-family: sans-serif; font-size: 15px">METODOLOGIAS</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/esa/organizacao" style="font-family: sans-serif; font-size: 15px">Organização</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/home" style="font-family: sans-serif; font-size: 15px">AVALIAÇÃO CONTINUADA</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<br>
<br>
<br>

<!-- Portfolio Grid Section -->
<section class="portfolio" id="portfolio">
    <p class="text-center"><img src="/img/brasaouepa.png" width="100px" height="100px" style="margin-top: -51px;" id="logo-header"></p>
    <div class="container">

        <h3 class="text-center text-uppercase text-secondary mb-0">Curso de Graduação em Fisioterapia - UEPA</h3>
        <br>
        <br>
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <img class="img-fluid" src="/img/metodologias/esa1.jpg" alt="">
            </div>
            <div class="col-md-6 col-lg-4">
                <img class="img-fluid" src="/img/metodologias/esa2.jpg" alt="">
            </div>
            <div class="col-md-6 col-lg-4">
                <img class="img-fluid" src="/img/metodologias/esa3.jpg" alt="">
            </div>

        </div>
    </div>
</section>

<!-- About Section -->
<section class="bg-secondary text-white mb-0" id="about">
    <div class="container">
        <h3 class="text-center text-uppercase text-white" style="font-size: 15px">Quem Somos</h3>
        <div class="row">
            <p class="lead" align="justify" style="font-family: sans-serif; font-size: 15px">Somos um sistema de avaliação  continuada acerca das metodologias ativas de ensino-aprendizagem adotadas pelo novo currículo  do Curso de Fisioterapia da Universidade do Estado  do Pará. Desta forma, contamos com sua colaboração para a avaliação e melhoria  deste processo. Desde já, nosso muito obrigado.</p>
        </div>
        <div class="text-center mt-4">
            <a class="btn btn-md btn-outline-light" href="/home" style="font-family: sans-serif;">
                <b>AVALIAÇÃO CONTINUADA</b>
            </a>
        </div>
    </div>
</section>

<!-- Contact Section -->


<!-- Footer -->

<div class="copyright py-4 text-center text-white">
    <div class="container">
        <small>Copyright &copy; Todos os Direitos Reservados a Milene Duarte 2018</small>
    </div>
</div>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>

<!-- Portfolio Modals -->

<!-- Bootstrap core JavaScript -->
<script src="/bower_components/bootswatch-dist/vendor/jquery/jquery.min.js"></script>
<script src="/bower_components/bootswatch-dist/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="/bower_components/bootswatch-dist/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="/bower_components/bootswatch-dist/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="/bower_components/bootswatch-dist/js/jqBootstrapValidation.js"></script>
<script src="/bower_components/bootswatch-dist/js/contact_me.js"></script>

<!-- Custom scripts for this template -->
<script src="/bower_components/bootswatch-dist/js/freelancer.min.js"></script>

</body>

</html>
