@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Início</a></li>
                    <li class="breadcrumb-item"><a href="{{route('discente')}}">Perfil do usuário</a></li>
                    <li class="breadcrumb-item active">Detalhe Avaliação</li>
                </ol>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Avaliação</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row-fluid">
                            <p class="text-center"><img src="/img/uepa.jpg" width="100px" height="100px" id="logo-header"></p>
                            <h5 class="text-center"><b>Universidade do Estado do Pará</b></h5>
                            <h5 class="text-center"><b>Curso de Graduação em Fisioterapia</b></h5>
                            <h5 class="text-center"><b>Campus XII - Santarém</b></h5>
                            <br>

                            <span class="pull-right">
                                        <button class="btn btn-info" type="button"  value="Imprimir" onclick="window.print();"><i class="glyphicon glyphicon-print"></i></button>
                                    </span>
                            <div class="span8">
                                <strong><h4><b>Dados Usuário:</b></h4></strong><br>
                                @foreach($dadosAvaliacao as $dados)
                                    <b>Código:</b> {{$dados->id}} <br>
                                @endforeach
                                @foreach($dadosUsuario as $dados)
                                    <b>Nome:</b> {{$dados->nome}} <br>
                                    <b>Email: </b>{{$dados->email}} <br>
                                    <b>Matricula: </b>{{$dados->matricula}} <br>
                                    <b>Idade: </b>{{$dados->idade}} <br>
                                    <b>Gênero: </b>{{$dados->sexo}} <br>

                                @if($dados->tipo_usuario_id == 1)
                                    <b>Tipo de Usuário:</b> Discente <br>
                                @elseif($dados->tipo_usuario_id == 2)
                                    <b>Tipo de Usuário:</b> Administrador <br>
                                @elseif($dados->tipo_usuario_id == 3)
                                    <b>Tipo de Usuário:</b> Docente <br>
                                @endif
                                @endforeach
                                <br>
                                <h5 class="text-center"><b>INSTRUMENTO DE AVALIAÇÃO CONTINUADA SOBRE METODOLOGIAS ATIVAS DE ENSINO-APRENDIZAGEM</b></h5>

                                <br>

                                @foreach($dadosAvaliacao as $dados)
                                    <div>
                                        <label class="col-md-12" >
                                            <p align="justify">
                                                1) As metodologias ativas de ensino, no curso de fisioterapia da UEPA, são estratégias que permitem a busca pelo conhecimento mais amplo e profundo, deixando o aluno com maior autonomia para estudar sobre o tema abordado.
                                            </p>
                                        </label>
                                        @if($dados->perg1 == 1)
                                            <p>Concordo totalmente</p>
                                        @elseif($dados->perg1 == 2)
                                            <p>Concordo parcialmente</p>
                                        @elseif($dados->perg1 == 3)
                                            <p>Não sei</p>
                                        @elseif($dados->perg1 == 4)
                                            <p>Discordo parcialmente</p>
                                        @elseif($dados->perg1 == 5)
                                            <p>Discordo totalmente</p>
                                        @endif
                                    </div>

                                    <div>
                                            <label class="col-md-12" >
                                                <p align="justify">
                                                    2) As metodologias ativas abordadas no curso de fisioterapia da UEPA estimulam uma postura ativa do aluno e o seu raciocínio lógico.
                                                </p>
                                            </label>
                                        @if($dados->perg2 == 1)
                                            <p>Concordo totalmente</p>
                                        @elseif($dados->perg2 == 2)
                                            <p>Concordo parcialmente</p>
                                        @elseif($dados->perg2 == 3)
                                            <p>Não sei</p>
                                        @elseif($dados->perg2 == 4)
                                            <p>Discordo parcialmente</p>
                                        @elseif($dados->perg2 == 5)
                                            <p>Discordo totalmente</p>
                                        @endif
                                    </div>

                                    <div>
                                        <label class="col-md-12" >
                                            <p align="justify">
                                                3) A relação professor-aluno no curso de fisioterapia da UEPA ocorre com maior proximidade, propiciando segurança e atingindo os resultados esperados pelo método.
                                            </p>
                                        </label>
                                        @if($dados->perg3 == 1)
                                            <p>Concordo totalmente</p>
                                        @elseif($dados->perg3 == 2)
                                            <p>Concordo parcialmente</p>
                                        @elseif($dados->perg3 == 3)
                                            <p>Não sei</p>
                                        @elseif($dados->perg3 == 4)
                                            <p>Discordo parcialmente</p>
                                        @elseif($dados->perg3 == 5)
                                            <p>Discordo totalmente</p>
                                        @endif
                                    </div>

                                    <div>
                                        <label class="col-md-12" >
                                            <p align="justify">
                                                4)No novo currículo do curso de fisioterapia da UEPA, a integração do morfofuncional, tutorial, atividade integrada e habilidades ocorre de forma coesa, atendendo os objetivos do método.
                                            </p>
                                        </label>
                                        @if($dados->perg4 == 1)
                                            <p>Concordo totalmente</p>
                                        @elseif($dados->perg4 == 2)
                                            <p>Concordo parcialmente</p>
                                        @elseif($dados->perg4 == 3)
                                            <p>Não sei</p>
                                        @elseif($dados->perg4 == 4)
                                            <p>Discordo parcialmente</p>
                                        @elseif($dados->perg4 == 5)
                                            <p>Discordo totalmente</p>
                                        @endif
                                    </div>

                                    <div>
                                        <label class="col-md-12" >
                                            <p align="justify">
                                                5) Os professores do curso de fisioterapia da UEPA possuem habilidades específicas acerca das metodologias ativas de ensino, impactando diretamente no sucesso das atividades desenvolvidas.
                                            </p>
                                        </label>
                                        @if($dados->perg5 == 1)
                                            <p>Concordo totalmente</p>
                                        @elseif($dados->perg5 == 2)
                                            <p>Concordo parcialmente</p>
                                        @elseif($dados->perg5 == 3)
                                            <p>Não sei</p>
                                        @elseif($dados->perg5 == 4)
                                            <p>Discordo parcialmente</p>
                                        @elseif($dados->perg5 == 5)
                                            <p>Discordo totalmente</p>
                                        @endif
                                    </div>

                                    <div>
                                        <label class="col-md-12" >
                                            <p align="justify">
                                                6) A execução do conteúdo planejado sobre os eixos temáticos, utilizado no curso de fisioterapia da UEPA, interfere diretamente no aprendizado, uma vez que ao sofrer alteração pode influenciar no aprendizado do eixo em questão.
                                            </p>
                                        </label>
                                        @if($dados->perg6 == 1)
                                            <p>Concordo totalmente</p>
                                        @elseif($dados->perg6 == 2)
                                            <p>Concordo parcialmente</p>
                                        @elseif($dados->perg6 == 3)
                                            <p>Não sei</p>
                                        @elseif($dados->perg6 == 4)
                                            <p>Discordo parcialmente</p>
                                        @elseif($dados->perg6 == 5)
                                            <p>Discordo totalmente</p>
                                        @endif
                                    </div>

                                    <div>
                                        <label class="col-md-12" >
                                            <p align="justify">
                                                7) A personalidade do aluno do curso de Fisioterapia da UEPA influencia diretamente na sua interação com o grupo e na sua avaliação, como por exemplo, em um tutorial.
                                            </p>
                                        </label>
                                        <label class="col-md-12">  </label>
                                        @if($dados->perg7 == 1)
                                            <p>Concordo totalmente</p>
                                        @elseif($dados->perg7 == 2)
                                            <p>Concordo parcialmente</p>
                                        @elseif($dados->perg7 == 3)
                                            <p>Não sei</p>
                                        @elseif($dados->perg7 == 4)
                                            <p>Discordo parcialmente</p>
                                        @elseif($dados->perg7 == 5)
                                            <p>Discordo totalmente</p>
                                        @endif
                                    </div>

                                    <div>
                                        <label class="col-md-12" >
                                            <p align="justify">
                                                8) A ausência de componentes curriculares específicos ministrados através de aulas tradicionais gera insegurança no aluno do novo currículo do curso de fisioterapia da UEPA.
                                            </p>
                                        </label>
                                        @if($dados->perg8 == 1)
                                            <p>Concordo totalmente</p>
                                        @elseif($dados->perg8 == 2)
                                            <p>Concordo parcialmente</p>
                                        @elseif($dados->perg8 == 3)
                                            <p>Não sei</p>
                                        @elseif($dados->perg8 == 4)
                                            <p>Discordo parcialmente</p>
                                        @elseif($dados->perg8 == 5)
                                            <p>Discordo totalmente</p>
                                        @endif
                                    </div>

                                    <div>
                                        <label class="col-md-12" >
                                            <p align="justify">
                                                9) O tempo de estudo extraclasse tanto para o discente como para o docente do curso de fisioterapia da UEPA é superior ao tempo que era dedicado ao método tradicional de ensino.
                                            </p>
                                        </label>
                                        @if($dados->perg9 == 1)
                                            <p>Concordo totalmente</p>
                                        @elseif($dados->perg9 == 2)
                                            <p>Concordo parcialmente</p>
                                        @elseif($dados->perg9 == 3)
                                            <p>Não sei</p>
                                        @elseif($dados->perg9 == 4)
                                            <p>Discordo parcialmente</p>
                                        @elseif($dados->perg9 == 5)
                                            <p>Discordo totalmente</p>
                                        @endif
                                    </div>

                                    <div>
                                        <label class="col-md-12" >
                                            <p align="justify">
                                                10) No curso de fisioterapia da UEPA, os envolvidos no processo de ensino, professor e aluno, são esclarecidos quanto as normas das metodologias ativas no processo ensino-aprendizagem.
                                            </p>
                                        </label>
                                        @if($dados->perg10 == 1)
                                            <p>Concordo totalmente</p>
                                        @elseif($dados->perg10 == 2)
                                            <p>Concordo parcialmente</p>
                                        @elseif($dados->perg10 == 3)
                                            <p>Não sei</p>
                                        @elseif($dados->perg10 == 4)
                                            <p>Discordo parcialmente</p>
                                        @elseif($dados->perg10 == 5)
                                            <p>Discordo totalmente</p>
                                        @endif
                                    </div>
                                    <div>
                                        <label class="col-md-12">Comentários</label>
                                        @if($dados->observacao == null)
                                            <p>Sem Comentários</p>
                                        @elseif($dados->observacao !== null )
                                            <p>{{$dados->observacao}}</p>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
