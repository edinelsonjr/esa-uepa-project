@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Início</a></li>
                    <li class="breadcrumb-item"><a href="{{route('discente')}}">Perfil do usuário</a></li>
                    <li class="breadcrumb-item active">Avaliação</li>
                </ol>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title" ALIGN="center">INSTRUMENTO DE AVALIAÇÃO CONTINUADA - IAC</h3>
                    </div>
                    <div class="panel-body">
                        <p class="text-center"><img src="/img/brasaouepa.png" width="100px" height="100px" id="logo-header"></p>
                        <h5 class="text-center"><b>Universidade do Estado do Pará</b></h5>
                        <h5 class="text-center"><b>Curso de Graduação em Fisioterapia</b></h5>
                        <h5 class="text-center"><b>Campus XII - Santarém</b></h5>
                        <br>

                        <p align="justify" style="font-family: 'arial'; font-size: 14px">Gostaria de contar com sua colaboração ao responder o instrumento de avaliação abaixo, sobre o método de ensino através de metodologias ativas, implantado recentemente no Curso de Graduação em Fisioterapia da Universidade do Estado do Pará. Marque a opção que melhor expressa sua opinião, e sinta-se à vontade para sugestões no campo destinado para comentários ao final do instrumento.</p>
                        <br>
                        <form method="POST" action="{{ route('discente.saveAvaliacao') }}">
                            {{ csrf_field() }}

                            <input type="hidden" name="idUsuarioAnoVigencia" value="{{$idUsuarioAnoVigencia}}">
                            <fieldset>
                                <div class="form-group{{ $errors->has('perg1') ? ' has-error' : '' }}">
                                    <label for="perg1" class="col-md-12 control-label" align="justify" align="justify">1) As metodologias ativas de ensino, no curso de fisioterapia da UEPA, são estratégias que permitem a busca pelo conhecimento mais amplo e profundo, deixando o aluno com maior autonomia para estudar sobre o tema abordado. </label>
                                    <select name="perg1" class="form-control" id="perg1" placeholder="Selecione a sua opção">
                                        <option value="#">Selecione o tipo</option>
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('perg1'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('perg1') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group{{ $errors->has('perg2') ? ' has-error' : '' }}">
                                    <label for="perg2" class="col-md-12 control-label" align="justify">2) As metodologias ativas abordadas no curso de fisioterapia da UEPA estimulam uma postura ativa do aluno e o seu raciocínio lógico.   </label>
                                    <select name="perg2" class="form-control" id="perg2" placeholder="Selecione a sua opção">
                                        <option value="#">Selecione o tipo</option>
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('perg2'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('perg2') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group{{ $errors->has('perg3') ? ' has-error' : '' }}">
                                    <label for="perg3" class="col-md-12 control-label" align="justify">3) A relação professor-aluno no curso de fisioterapia da UEPA ocorre com maior proximidade, oferecendo segurança e atingindo os resultados esperados pelo método. </label>
                                    <select name="perg3" class="form-control" id="perg3" placeholder="Selecione a sua opção">
                                        <option value="#">Selecione o tipo</option>
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('perg3'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('perg3') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group{{ $errors->has('perg4') ? ' has-error' : '' }}">
                                    <label for="perg4" class="col-md-12 control-label" align="justify">4) No novo currículo do curso de fisioterapia da UEPA, a integração do morfofuncional, tutorial, atividade integrada e habilidades ocorre de forma coesa, atendendo os objetivos do método. </label>
                                    <select name="perg4" class="form-control" id="perg4" placeholder="Selecione a sua opção">
                                        <option value="#">Selecione o tipo</option>
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('perg4'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('perg4') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group{{ $errors->has('perg5') ? ' has-error' : '' }}">
                                    <label for="perg5" class="col-md-12 control-label" align="justify">5) Os professores do curso de fisioterapia da UEPA possuem habilidades específicas acerca das metodologias ativas de ensino, impactando diretamente no sucesso das atividades desenvolvidas. </label>
                                    <select name="perg5" class="form-control" id="perg5" placeholder="Selecione a sua opção">
                                        <option value="#">Selecione o tipo</option>
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('perg5'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('perg5') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group{{ $errors->has('perg6') ? ' has-error' : '' }}">
                                    <label for="perg6" class="col-md-12 control-label" align="justify">6) A execução do conteúdo planejado sobre os eixos temáticos, utilizado no curso de fisioterapia da UEPA, interfere diretamente no aprendizado, uma vez que ao sofrer alteração pode influenciar no aprendizado do eixo em questão. </label>
                                    <select name="perg6" class="form-control" id="perg6" placeholder="Selecione a sua opção">
                                        <option value="#">Selecione o tipo</option>
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('perg6'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('perg6') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group{{ $errors->has('perg7') ? ' has-error' : '' }}">
                                    <label for="perg7" class="col-md-12 control-label" align="justify">7) A personalidade do aluno do curso de Fisioterapia da UEPA influencia diretamente na sua interação com o grupo e na sua avaliação, como por exemplo, em um tutorial. </label>
                                    <select name="perg7" class="form-control" id="perg7" placeholder="Selecione a sua opção">
                                        <option value="#">Selecione o tipo</option>
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('perg7'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('perg7') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>


                            <fieldset>
                                <div class="form-group{{ $errors->has('perg8') ? ' has-error' : '' }}">
                                    <label for="perg8" class="col-md-12 control-label" align="justify">8) A ausência de componentes curriculares específicos ministrados através de aulas tradicionais gera insegurança no aluno do novo currículo do curso de fisioterapia da UEPA.</label>
                                    <select name="perg8" class="form-control" id="perg8" placeholder="Selecione a sua opção">
                                        <option value="#">Selecione o tipo</option>
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('perg8'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('perg8') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>


                            <fieldset>
                                <div class="form-group{{ $errors->has('perg9') ? ' has-error' : '' }}">
                                    <label for="perg9" class="col-md-12 control-label" align="justify">9) O tempo de estudo extraclasse tanto para o discente como para o docente do curso de fisioterapia da UEPA é superior ao tempo que era dedicado ao método tradicional de ensino.</label>
                                    <select name="perg9" class="form-control" id="perg9" placeholder="Selecione a sua opção">
                                        <option value="#">Selecione o tipo</option>
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('perg9'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('perg9') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>


                            <fieldset>
                                <div class="form-group{{ $errors->has('perg10') ? ' has-error' : '' }}">
                                    <label for="perg10" class="col-md-12 control-label" align="justify">10) No curso de fisioterapia da UEPA, os envolvidos no processo de ensino, professor e aluno, são esclarecidos quanto as normas das metodologias ativas no processo ensino-aprendizagem. </label>
                                    <select name="perg10" class="form-control" id="perg10" placeholder="Selecione a sua opção">
                                        <option value="#">Selecione o tipo</option>
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('perg10'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('perg10') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group{{ $errors->has('observacao') ? ' has-error' : '' }}">
                                    <label for="observacao" class="col-md-12 control-label" align="justify">Comentários:</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" name="observacao"rows="3"></textarea>
                                    @if ($errors->has('observacao'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('observacao') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>


                                <button type="submit" class="btn btn-info col-md-offset-4  col-md-4">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
