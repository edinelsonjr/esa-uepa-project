@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Início</a></li>
                    <li class="breadcrumb-item"><a href="{{route('discente')}}">Perfil do usuário</a></li>
                    <li class="breadcrumb-item active">Avaliação</li>
                </ol>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">AVALIAÇÃO DAS METODOLOGIAS ATIVAS DE ENSINO </h3>
                    </div>
                    <div class="panel-body">
                        Gostaria de contar com a sua colaboração respondendo a este questionário com o máximo de atenção e sinceridade. Desde já agradecemos!
                        <form method="POST" action="{{ route('discente.editAvaliacaoSave') }}">
                            {{ csrf_field() }}

                            <input type="hidden" name="idUsuarioAnoVigencia" value="{{$idUsuarioAnoVigencia}}">
                            <fieldset>
                                <div class="form-group{{ $errors->has('perg1') ? ' has-error' : '' }}">
                                    <label for="resp_perg1" class="col-md-12 control-label">1) As metodologias ativas de ensino, no curso de fisioterapia da UEPA, são estratégias que permitem a busca pelo conhecimento mais amplo e profundo, deixando o aluno com maior autonomia para estudar sobre o tema abordado.</label>
                                    <select name="resp_perg1" class="form-control" id="resp_perg1" placeholder="Selecione a sua opção">
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('perg1'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('perg1') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group{{ $errors->has('perg2') ? ' has-error' : '' }}">
                                    <label for="resp_perg2" class="col-md-12 control-label">2) As metodologias ativas abordadas no curso de fisioterapia da UEPA estimulam uma postura ativa do aluno e o seu raciocínio lógico.  </label>
                                    <select name="resp_perg2" class="form-control" id="resp_perg2" placeholder="Selecione a sua opção">
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('resp_perg2'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('resp_perg2') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group{{ $errors->has('resp_perg3') ? ' has-error' : '' }}">
                                    <label for="resp_perg3" class="col-md-12 control-label">3) A relação professor-aluno no curso de fisioterapia da UEPA ocorre de forma horizontal com maior proximidade, propiciando segurança e atingindo os resultados esperados pelo método. </label>
                                    <select name="resp_perg3" class="form-control" id="resp_perg3" placeholder="Selecione a sua opção">
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('resp_perg3'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('resp_perg3') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group{{ $errors->has('resp_perg4') ? ' has-error' : '' }}">
                                    <label for="resp_perg4" class="col-md-12 control-label">4) No novo currículo do curso de fisioterapia da UEPA, a integração do morfofuncional, tutorial, atividade integrada e habilidades ocorre de forma coesa, atendendo os objetivos do método. </label>
                                    <select name="resp_perg4" class="form-control" id="resp_perg4" placeholder="Selecione a sua opção">
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('resp_perg4'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('resp_perg4') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group{{ $errors->has('resp_perg5') ? ' has-error' : '' }}">
                                    <label for="resp_perg5" class="col-md-12 control-label">5) Os professores do curso de fisioterapia da UEPA possuem habilidades específicas acerca das metodologias ativas de ensino, impactando diretamente no sucesso das atividades desenvolvidas. </label>
                                    <select name="resp_perg5" class="form-control" id="resp_perg5" placeholder="Selecione a sua opção">
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('resp_perg5'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('resp_perg5') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group{{ $errors->has('resp_perg6') ? ' has-error' : '' }}">
                                    <label for="resp_perg6" class="col-md-12 control-label">6) O cronograma fechado sobre os eixos temáticos, utilizado no curso de fisioterapia da UEPA, interfere diretamente no aprendizado, uma vez que ao sofrer alteração pode influenciar no aprendizado do eixo em questão. </label>
                                    <select name="resp_perg6" class="form-control" id="resp_perg6" placeholder="Selecione a sua opção">
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('resp_perg6'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('resp_perg6') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group{{ $errors->has('resp_perg7') ? ' has-error' : '' }}">
                                    <label for="resp_perg7" class="col-md-12 control-label">7) A personalidade do aluno do curso de Fisioterapia da UEPA influencia diretamente na sua interação com o grupo e na sua avaliação, como por exemplo, em um tutorial. </label>
                                    <select name="resp_perg7" class="form-control" id="resp_perg7" placeholder="Selecione a sua opção">
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('resp_perg7'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('resp_perg7') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>


                            <fieldset>
                                <div class="form-group{{ $errors->has('resp_perg8') ? ' has-error' : '' }}">
                                    <label for="resp_perg8" class="col-md-12 control-label">8) A ausência de componentes curriculares específicos ministrados através de aulas tradicionais gera insegurança no aluno do novo currículo do curso de fisioterapia da UEPA.</label>
                                    <select name="resp_perg8" class="form-control" id="resp_perg8" placeholder="Selecione a sua opção">
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('resp_perg8'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('resp_perg8') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>


                            <fieldset>
                                <div class="form-group{{ $errors->has('resp_perg9') ? ' has-error' : '' }}">
                                    <label for="resp_perg9" class="col-md-12 control-label">9) O tempo de estudo extraclasse tanto para o discente como para o docente do curso de fisioterapia da UEPA é superior ao tempo que era dedicado ao método tradicional de ensino.</label>
                                    <select name="resp_perg9" class="form-control" id="resp_perg9" placeholder="Selecione a sua opção">
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('resp_perg9'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('resp_perg9') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>


                            <fieldset>
                                <div class="form-group{{ $errors->has('resp_perg10') ? ' has-error' : '' }}">
                                    <label for="resp_perg10" class="col-md-12 control-label">10) No curso de fisioterapia da UEPA, os envolvidos no processo de ensino, professor e aluno, são esclarecidos quanto as diretrizes das metodologias ativas no processo ensino-aprendizagem. </label>
                                    <select name="resp_perg10" class="form-control" id="resp_perg10" placeholder="Selecione a sua opção">
                                        <option value="1">Concordo totalmente </option>
                                        <option value="2">Concordo parcialmente </option>
                                        <option value="3">Não sei </option>
                                        <option value="4">Discordo parcialmente </option>
                                        <option value="5">Discordo totalmente </option>
                                    </select>
                                    @if ($errors->has('resp_perg10'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('resp_perg10') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </fieldset>
                            @foreach($buscado as $busca)
                                <fieldset>
                                    <div class="form-group{{ $errors->has('observacao') ? ' has-error' : '' }}">
                                        <label for="observacao" class="col-md-12 control-label" align="justify">Comentários:</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" value="{{$busca->observacao}}" name="observacao" rows="3"></textarea>
                                        @if ($errors->has('observacao'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('observacao') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </fieldset>
                            @endforeach


                            <button type="submit" class="btn btn-info col-md-offset-4  col-md-4">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
