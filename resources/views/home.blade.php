@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div class="jumbotron">



                            <h1 class="display-1">Bem vindo!</h1>
                            <hr class="my-2">
                            <p class="lead">
                                <a class="btn btn-primary btn-lg col-md-offset-8" href="/perfil" role="button">Acesar meu perfil <span class="glyphicon glyphicon-chevron-right"></span></a>
                            </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
