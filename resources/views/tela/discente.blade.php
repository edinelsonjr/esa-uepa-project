@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
                            <li class="breadcrumb-item active">Perfil do usuário</li>
                        </ol>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Perfil do Usuario</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row-fluid">
                                    <span class="pull-right">
                                        <a class="btn btn-warning" href="/esa/portal_usuario/edit/" type="button" data-toggle="tooltip" title="Editar Minhas Informações"><i class="glyphicon glyphicon-pencil"></i></a>
                                    </span>
                                    <div class="span8">
                                        @foreach($queryDadosUser as $dados)
                                        <strong><h5>Meus dados:</h5></strong><br>
                                        <table class="table table-condensed table-responsive table-user-information">
                                            <tbody>
                                            <tr>
                                                <td><b>Tipo Perfil:</b></td>
                                                @if($dados->tipoId == 1)

                                                    <td>Discente</td>

                                                @elseif($dados->tipoId == 2)
                                                    <td>Admnistrador</td>
                                                @elseif($dados->tipoId == 3)
                                                    <td>Docente</td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td><b>Email:</b></td>
                                                <td>{{$dados->email}}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Matricula:</b></td>
                                                <td>{{$dados->matricula}}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Status:</b></td>
                                                @if($dados->status == 1)

                                                    <td><span class="badge badge-success">Ativo</span></td>

                                                @elseif($dados->tipoId == 0)
                                                    <td><span class="badge badge-danger">Inativo</span></td>
                                                @endif
                                            </tr>
                                            </tbody>
                                        </table>
                                            @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Minhas Avaliações Disponiveis</h3>
                    </div>
                    <div class="panel-body">

                        @if($statusAvaliacaoDisponivel == 0)
                            Você ainda não está vinculado a um Grupo de Avaliação.
                        @elseif($statusAvaliacaoDisponivel == 1)
                            @foreach($queryDadosAnoVigenciaDisponivel as $dados)
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"></h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row-fluid">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Ano Turma</th>
                                                        <th scope="col">Periodo Vigência</th>
                                                        <th scope="col">-</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr class="table-active">
                                                        <td>{{$dados->anoTurma}}</td>
                                                        <td>{{$dados->vigenciaAvaliacao}}</td>
                                                        @if($dadosAvaliacao == null)
                                                            <td>
                                                                <a class="btn btn-success" type="button" href="{{route('discente.createAvaliacao', $dados->id)}}" data-toggle="tooltip" title="Preencher Avaliação"><i class="glyphicon glyphicon-plus-sign"></i></a>
                                                            </td>
                                                        @elseif($dadosAvaliacao !== null)
                                                            <td>
                                                                <a class="btn btn-success" type="button" href="{{route('discente.detalheAvaliacao', $dadosAvaliacao)}}" data-toggle="tooltip" title="Visualizar avaliação"><i class="glyphicon glyphicon-eye-open"></i></a>
                                                                <a class="btn btn-info" type="button" href="{{route('discente.editAvaliacao', $dadosAvaliacao)}}" data-toggle="tooltip" title="Editar avaliação"><i class="glyphicon glyphicon-pencil"></i></a>
                                                                <a class="btn btn-danger" type="button" href="{{route('discente.deleteAvaliacao', $dadosAvaliacao)}}" data-toggle="tooltip" title="Excluir avaliação"><i class="glyphicon glyphicon-trash"></i></a>

                                                            </td>
                                                        @endif
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif


                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Minhas Avaliações Respondidas</h3>
                    </div>
                    <div class="panel-body">
                        @foreach($queryDadosAnoVigencia as $dados)
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h3 class="panel-title"></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row-fluid">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Ano Turma</th>
                                                    <th scope="col">Periodo Vigência</th>
                                                    <th scope="col">-</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr class="table-active">
                                                    <td>{{$dados->anoTurma}}</td>
                                                    <td>{{$dados->vigenciaAvaliacao}}</td>
                                                    <td><a class="btn btn-success" type="button" href="{{route('discente.detalheAvaliacao', $dados->idAvaliacao)}}" data-toggle="tooltip" data-original-title="Visualizar avaliação"><i class="glyphicon glyphicon-eye-open"></i></a></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>



            </div>
        </div>
    </div>
@endsection
