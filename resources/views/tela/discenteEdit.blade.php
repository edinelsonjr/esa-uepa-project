@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('discente')}}">Portal do Usuário</a></li>
                    <li class="breadcrumb-item active">Editar</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Usuario</div>
                    <div class="panel-body">
                        @foreach($buscado as $busca)
                            <form action="{{route('usuario.updateEdit', $busca->id)}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="put">
                                <fieldset>
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-4 control-label">Nome:</label>
                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="name" value="{{$busca->name}}">
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                </fieldset>
                                <br>
                                <fieldset>
                                <div class="form-group{{ $errors->has('matricula') ? ' has-error' : '' }}">
                                    <label for="matricula" class="col-md-4 control-label">Matricula</label>

                                    <div class="col-md-6">
                                        <input id="matricula" type="text" class="form-control" name="matricula" value="{{ $busca->matricula }}" required autofocus>
                                        @if ($errors->has('matricula'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('matricula') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                </fieldset>
                                <br>
                                <fieldset>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">E-Mail</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ $busca->email }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                </fieldset>


                                <br>
                                <fieldset>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Salvar
                                        </button>
                                    </div>
                                </div>
                                </fieldset>
                            </form>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
