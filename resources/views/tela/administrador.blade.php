@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active">Portal Administrador</li>
                </ol>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-tasks"></span> DashBoard de Administração ESA</h3>
                    </div>

                    <div class="panel-body">
                        <a href="{{route('tipoUsuario.index')}}" class="btn btn-primary btn-md btn-block" role="button"><span class="glyphicon glyphicon-asterisk"></span> <br/> Tipos Usuários</a>

                        <a href="{{route('usuario.index')}}" class="btn btn-success btn-md btn-block" role="button"><span class="glyphicon glyphicon-user"></span> <br/>Usuários</a>

                        <a href="{{route('ano_vigencia.index')}}" class="btn btn-info btn-md btn-block" role="button"><span class="glyphicon glyphicon-calendar"></span> <br/>Anos Vigência</a>

                        <a href="#" class="btn btn-warning btn-md btn-block" role="button"><span class="glyphicon glyphicon-stats"></span> <br/>Estatisticas e relatório</a>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
