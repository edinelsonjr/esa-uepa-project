<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
Inicio Gerenciamento de Tipo Usuario
 **/

/**
Fim Gerenciamento de Tipo Usuario
 **/

/**
Inicio Gerenciamento de Tipo de Usuario
 **/

Route::get('esa/tipo_usuario/index', 'TipoUsuarioController@index')->name('tipoUsuario.index');

Route::get('esa/tipo_usuario/edit/{id}', 'TipoUsuarioController@edit')->name('tipoUsuario.edit');

Route::post('esa/tipo_usuario/save', 'TipoUsuarioController@save')->name('tipoUsuario.save');

Route::put('esa/tipo_usuario/edit/save/{id}', 'TipoUsuarioController@editSave')->name('tipo_usuario.save_edit');

Route::get('esa/tipo_usuario/edit/{id}', 'TipoUsuarioController@edit')->name('tipoUsuario.edit');

Route::get('esa/tipo_usuario/remove/{id}', 'TipoUsuarioController@remove')->name('tipoUsuario.remove');


/**
Fim Gerenciamento de Usuario
 **/
Auth::routes();

/**Inicio gerenciamento Usuarios**/
Route::get('esa/usuario/index', 'UsuarioController@index')->name('usuario.index');

Route::get('esa/usuario/edit/{id}', 'UsuarioController@edit')->name('usuario.edit');

Route::get('esa/usuario/cadastro_teste/', 'UsuarioController@cadastroTeste')->name('cadastroTeste');

Route::post('esa/usuario/save', 'UsuarioController@save')->name('usuario.save');


Route::post('esa/usuario/cadastro_teste/save', 'UsuarioController@saveCadastroTeste')->name('cadastroTeste.save');

Route::get('esa/usuario/vincular/ano-vigencia/{id}', 'UsuarioController@vincularCadastroTeste')->name('vincular.cadastroTeste');



Route::put('esa/usuario/edit/save/{id}', 'UsuarioController@update')->name('usuario.update');

Route::put('esa/portal_usuario/edit/save/{id}', 'UsuarioController@updateEdit')->name('usuario.updateEdit');

Route::get('esa/usuario/edit/{id}', 'UsuarioController@edit')->name('usuario.edit');

Route::get('esa/usuario/remove/{id}', 'UsuarioController@remove')->name('usuario.remove');

/**fIM DO GERENCIAMENTO DE USUARIOS**/
Route::get('/home', 'HomeController@index')->name('home');


Route::get('/esa/portal_administrador', 'GerenciamentoController@administrador')->name('administrador');

Route::get('/esa/portal_usuario', 'GerenciamentoController@discente')->name('discente');

Route::get('/esa/portal_docente', 'GerenciamentoController@docente')->name('docente');


Route::get('/perfil/', 'GerenciamentoController@perfil')->name('perfil');

Route::get('/esa/ano_vigencia/index', 'AnoVigenciaController@index')->name('ano_vigencia.index');

Route::get('/esa/ano_vigencia/detail/{id}', 'AnoVigenciaController@detail')->name('ano_vigencia.detail');


Route::get('/esa/ano_vigencia/vincular_usuario/{id}', 'UsuarioAnoVigenciaController@vincularUsuario')->name('ano_vigencia.vincularUsuario');

Route::post('/esa/ano_vigencia/vincular_usuario/save', 'UsuarioAnoVigenciaController@vincularUsuarioSave')->name('ano_vigencia.vincularUsuarioSave');

Route::post('/esa/ano_vigencia/save', 'AnoVigenciaController@save')->name('ano_vigencia.save');

Route::get('/esa/ano_vigencia/desvincular_usuario/{id}', 'UsuarioAnoVigenciaController@desvincularUsuario')->name('ano_vigencia.desvincularUsuario');

Route::get('/esa/ano_vigencia/desativar/{id}', 'AnoVigenciaController@desativar')->name('ano_vigencia.desativar');

Route::get('/esa/ano_vigencia/ativar/{id}', 'AnoVigenciaController@ativar')->name('ano_vigencia.ativar');

Route::get('/esa/ano_vigencia/fechar/{id}', 'AnoVigenciaController@fechar')->name('ano_vigencia.fechar');

Route::get('/esa/ano_vigencia/abrir/{id}', 'AnoVigenciaController@abrir')->name('ano_vigencia.abrir');

Route::get('/teste', 'AnoVigenciaController@teste')->name('teste');

Route::get('/esa/portal_usuario/avaliacao/{id}', 'AvaliacaoController@detalheAvaliacao')->name('discente.detalheAvaliacao');

Route::get('/esa/portal_usuario/avaliacao/create/{id}', 'AvaliacaoController@create')->name('discente.createAvaliacao');

Route::post('/esa/portal_usuario/avaliacao/save', 'AvaliacaoController@save')->name('discente.saveAvaliacao');

Route::get('/esa/portal_usuario/avaliacao/edit/{id}', 'AvaliacaoController@edit')->name('discente.editAvaliacao');

Route::post('/esa/portal_usuario/avaliacao/edit/save', 'AvaliacaoController@editSave')->name('discente.editAvaliacaoSave');

Route::get('/esa/portal_usuario/avaliacao/delete/{id}', 'GerenciamentoController@delete')->name('discente.deleteAvaliacao');

Route::get('/esa/portal_usuario/edit/', 'GerenciamentoController@editDiscente')->name('discente.edit');


