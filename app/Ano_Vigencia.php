<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ano_Vigencia extends Model
{
    use SoftDeletes ;
    protected $table = 'ano_vigencia';

    protected $fillable =
        [
            'ano',
            'vigencia',
            'status_ano_vigencia',
            'status_ativacao_grupo',
        ];

    /****************************************************************************
     * RELACIONAMENTO ENTRE AS TABELAS
     ****************************************************************************/

    public function usuarioTemAnoVigencia()
    {
        return $this->hasMany('App\Usuario_Tem_Ano_Vigencia');
    }
}
