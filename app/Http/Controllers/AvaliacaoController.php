<?php

namespace App\Http\Controllers;

use App\Avaliacao;
use App\Http\Controllers\Funcoes\FuncoesAuxiliaresController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AvaliacaoController extends Controller
{
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if (Auth::check()) {
            $idUser = Auth::id();

            $verificaDiscente = new FuncoesAuxiliaresController();

            $discente= $verificaDiscente->verificaDiscente($idUser);


            if ($discente == true){

                $idUsuarioAnoVigencia = $id;

                $dadosAnoVigencia = DB::table('ano_vigencia')
                    ->join('usuario_ano_vigencia', 'usuario_ano_vigencia.ano_vigencia_id', '=', 'ano_vigencia.id')
                    ->select('ano_vigencia.*')
                    ->where('usuario_ano_vigencia.id', '=', $idUsuarioAnoVigencia)
                    ->where('ano_vigencia.status_ano_vigencia', '=', 1)
                    ->get();

                $dadosAvaliacao = DB::table('avaliacao')
                    ->join('usuario_ano_vigencia', 'usuario_ano_vigencia.id', '=', 'avaliacao.usuario_id')
                    ->join('users', 'users.id', '=', 'usuario_ano_vigencia.usuario_id')
                    ->select('avaliacao.*')
                    ->where('avaliacao.usuario_id', '=', $idUsuarioAnoVigencia)
                    ->where('users.id', '=', $idUser)
                    ->where('avaliacao.status_avaliacao', '=', 1)
                    ->get();

                $dadosAvaliacao2 = DB::table('avaliacao')
                    ->join('usuario_ano_vigencia', 'usuario_ano_vigencia.id', '=', 'avaliacao.usuario_id')
                    ->join('users', 'users.id', '=', 'usuario_ano_vigencia.usuario_id')
                    ->select('avaliacao.*')
                    ->where('avaliacao.usuario_id', '=', $idUsuarioAnoVigencia)
                    ->where('users.id', '=', $idUser)
                    ->where('avaliacao.status_avaliacao', '=', 0)
                    ->get();


                if ($dadosAnoVigencia == []){
                    \Session::flash('flash_message', [
                        'msg' => "Você não pode mais cadastrar pois o grupo já está desativado",
                        'class' => "alert-danger"
                    ]);
                    return redirect(route('discente'));
                }elseif ($dadosAnoVigencia !== []){

                    if($dadosAvaliacao2 == []){

                        if ($dadosAvaliacao == []){
                            return view('avaliacao.create', compact('idUsuarioAnoVigencia'));
                        }elseif ($dadosAvaliacao !== '[]'){
                            \Session::flash('flash_message', [
                                'msg' => "Erro ao buscar pagina, por favor essa forma de acessar está errada!",
                                'class' => "alert-danger"
                            ]);
                            return redirect(route('discente'));
                        }

                    }elseif ($dadosAvaliacao2 !== []){
                        if ($dadosAvaliacao == []){
                            return view('avaliacao.create', compact('idUsuarioAnoVigencia'));
                        }elseif ($dadosAvaliacao !== []){
                            \Session::flash('flash_message', [
                                'msg' => "Erro ao buscar pagina, por favor essa forma de acessar está errada!",
                                'class' => "alert-danger"
                            ]);
                            return redirect(route('discente'));
                        }
                    }

                }

            }elseif ($discente == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }

        } else {
            return view('auth.login');
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        if (Auth::check()) {
            $idUser = Auth::id();

            $idUsuarioAnoVigenciaId = $request->idUsuarioAnoVigencia;



            $verificaDiscente = new FuncoesAuxiliaresController();



            $dadosAdmin1 = DB::table('users')
                ->join('tipo_user', 'users.tipo_usuario_id', '=', 'tipo_user.id')
                ->select('users.*', 'tipo_user.nome_tipo_usuario')
                ->where('users.id', '=', $idUser)
                ->where('tipo_user.id', '=', 1);

            $dadosAdmin = DB::table('users')
                ->join('tipo_user', 'users.tipo_usuario_id', '=', 'tipo_user.id')
                ->select('users.*', 'tipo_user.nome_tipo_usuario')
                ->where('users.id', '=', $idUser)
                ->where('tipo_user.id', '=', 3)
                ->union($dadosAdmin1)
                ->get();




            if ($dadosAdmin == null){
                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }elseif ($dadosAdmin !==  null){
                $perg1 =  $request->perg1;
                $perg2 =  $request->perg2;
                $perg3 =  $request->perg3;
                $perg4 =  $request->perg4;
                $perg5 =  $request->perg5;
                $perg6 =  $request->perg6;
                $perg7 =  $request->perg7;
                $perg8 =  $request->perg8;
                $perg9 =  $request->perg9;
                $perg10 =  $request->perg10;
                $observacao =  $request->observacao;


                $usuario = new Avaliacao();

                Avaliacao::create([
                    'resp_perg1' => $perg1,
                    'resp_perg2' => $perg2,
                    'resp_perg3' => $perg3,
                    'resp_perg4' => $perg4,
                    'resp_perg5' => $perg5,
                    'resp_perg6' => $perg6,
                    'resp_perg7' => $perg7,
                    'resp_perg8' => $perg8,
                    'resp_perg9' => $perg9,
                    'resp_perg10' => $perg10,
                    'observacao' => $observacao,
                    'status_avaliacao' => true,
                    'usuario_id' => $idUsuarioAnoVigenciaId,
                ]);
                \Session::flash('flash_message', [
                    'msg' => "Avaliação cadastrada com sucesso",
                    'class' => "alert-info"
                ]);
                return redirect(route('discente'));
            }
        } else {
            return view('auth.login');
        }
    }

    public function desativar($id){
        return "Chegoua aqui pra desativar";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()){

            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaDiscente($idUser);


            if ($admin == true){
                $buscado = DB::table('avaliacao')
                    ->where('id', $id)
                    ->get();

                $idUsuarioAnoVigencia = DB::table('avaliacao')
                    ->where('id', $id)
                    ->value('avaliacao.id');


                return view('avaliacao.edit', compact('buscado', 'idUsuarioAnoVigencia'));
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }
        }else{
            return view('auth.login');
        }
    }
    public function editSave(Request $request)
    {
        if (Auth::check()){
            $idUser = Auth::id();
            $verificaAdmin = new FuncoesAuxiliaresController();
            $admin = $verificaAdmin->verificaDiscente($idUser);
            $id = $request->idUsuarioAnoVigencia;
            if ($admin == true){
                \App\Avaliacao::find($id)->update($request->all());
                //caso o id não exista
                \Session::flash('flash_message', [
                    'msg' => "Alterações realizadas com sucesso",
                    'class' => "alert-info"
                ]);
                return redirect(route('discente'));
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }
        }else{
            return view('auth.login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function detalheAvaliacao($idAvaliacao){
        $idAvaliacaoUser = $idAvaliacao;
        $idUsers = Auth::id();


        $dadosAvaliacao = DB::table('avaliacao')
            ->join('usuario_ano_vigencia', 'usuario_ano_vigencia.id', '=', 'avaliacao.usuario_id')
            ->join('ano_vigencia', 'ano_vigencia.id', '=', 'usuario_ano_vigencia.ano_vigencia_id')
            ->join('users', 'users.id', '=', 'usuario_ano_vigencia.usuario_id')
            ->select(
                'avaliacao.id as id',
                'avaliacao.resp_perg1 as perg1',
                'avaliacao.resp_perg2 as perg2',
                'avaliacao.resp_perg3 as perg3',
                'avaliacao.resp_perg4 as perg4',
                'avaliacao.resp_perg5 as perg5',
                'avaliacao.resp_perg6 as perg6',
                'avaliacao.resp_perg7 as perg7',
                'avaliacao.resp_perg8 as perg8',
                'avaliacao.resp_perg9 as perg9',
                'avaliacao.resp_perg10 as perg10',
                'avaliacao.observacao as observacao',
                'avaliacao.usuario_id as usuario_id',
                'avaliacao.status_avaliacao as status_avaliacao',
                'ano_vigencia.ano as anoVigencia',
                'ano_vigencia.vigencia as vigencia'
            )
            ->where('avaliacao.id', '=', $idAvaliacaoUser)
            ->where('avaliacao.status_avaliacao', '=', 1)
            ->where('users.id', '=', $idUsers)
            ->get();


        if ($dadosAvaliacao == []){
            \Session::flash('flash_message', [
                'msg' => "Erro ao visualizar avaliação",
                'class' => "alert-danger"
            ]);
            return redirect(route('discente'));
        }elseif ($dadosAvaliacao !== []){


            $dadosUsuario = DB::table('usuario_ano_vigencia')
                ->join('avaliacao', 'avaliacao.usuario_id','=' ,'usuario_ano_vigencia.id')
                ->join('users', 'users.id','=', 'usuario_ano_vigencia.usuario_id')
                ->select(
                    'users.name as nome',
                    'users.matricula as matricula',
                    'users.email as email',
                    'users.idade as idade',
                    'users.sexo as sexo',
                    'users.tipo_usuario_id as tipo_usuario_id'
                )
                ->where('avaliacao.id', '=', $idAvaliacaoUser)
                ->get();


            return view('avaliacao.detail', compact('dadosAvaliacao', 'dadosUsuario'));
        }


    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
    }
}
