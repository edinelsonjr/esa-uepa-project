<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Funcoes\FuncoesAuxiliaresController;
use App\TipoUsuario;
use function Faker\Provider\pt_BR\check_digit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class TipoUsuarioController extends Controller
{
    //

    public function index(){

        if (Auth::check()) {
            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaAdministrador($idUser);




            if ($admin == true){
                $tipoUsuarios = TipoUsuario::all();

                $verificarTipoUsuario = 0;
                if ($tipoUsuarios == '[]'){

                    return view('tipo_usuario.index', compact('tipoUsuarios', 'verificarTipoUsuario'));
                }elseif ($tipoUsuarios !== '[]'){
                    $verificarTipoUsuario = 1;
                    return view('tipo_usuario.index', compact('tipoUsuarios', 'verificarTipoUsuario') );
                }

            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }

        } else {
            return view('auth.login');
        }




    }
    public function save(Request $request){
        if (Auth::check()) {
            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaAdministrador($idUser);




            if ($admin == true){
                $nomeTIpoUsuario = $request['nome_tipo_usuario'];
                $palavraMaiuscula = new Funcoes\FuncoesAuxiliaresController();
                $palavraMaiuscula = $palavraMaiuscula->passarMaiuscula($nomeTIpoUsuario);
                $statusBuscaNome = new Funcoes\FuncoesAuxiliaresController();
                $nomeTIpo = $palavraMaiuscula;
                $statusBusca = $statusBuscaNome->reconhecerDuplicidade($nomeTIpo, 2);
                if ($statusBusca == true){
                    if (TipoUsuario::create([
                        'nome_tipo_usuario' => $nomeTIpo,
                        'status_tipo_usuario' => 1,
                    ])){
                        \Session::flash('flash_message', [
                            'msg' => "Tipo de Usuario adicionado com sucesso",
                            'class' => "alert-success"
                        ]);
                        return redirect(route('tipoUsuario.index'));
                    }else{
                        \Session::flash('flash_message', [
                            'msg' => "Erro no cadastro de tipo de usuario",
                            'class' => "alert-danger"
                        ]);
                        return redirect(route('tipoUsuario.index'));
                    }
                }elseif ($statusBusca == false){
                    \Session::flash('flash_message', [
                        'msg' => "Tipo Usuario Ja cadastrado",
                        'class' => "alert-danger"
                    ]);
                    return redirect(route('tipoUsuario.index'));
                }

            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }

        } else {
            return view('auth.login');
        }


    }
    public function edit($id){
        if (Auth::check()){

            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaAdministrador($idUser);




            if ($admin == true){
                $buscado = DB::table('tipo_user')
                    ->where('id', $id)
                    ->get();
                return view('tipo_usuario.edit', compact('buscado'));
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }
        }else{
            return view('auth.login');
        }

    }

    public function editSave(Request $request, $id){
        if (Auth::check()){

            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaAdministrador($idUser);




            if ($admin == true){
                $nomeTIpoUsuario = $request['nome_tipo_usuario'];
                $palavraMaiuscula = new Funcoes\FuncoesAuxiliaresController();
                $palavraMaiuscula = $palavraMaiuscula->passarMaiuscula($nomeTIpoUsuario);
                $statusBuscaNome = new Funcoes\FuncoesAuxiliaresController();
                $nomeTIpo = $palavraMaiuscula;
                $statusBusca = $statusBuscaNome->reconhecerDuplicidade($nomeTIpo, 2);
                if ($statusBusca == true){
                    \App\TipoUsuario::find($id)->update($request->all());
                    //caso o id não exista
                    \Session::flash('flash_message', [
                        'msg' => "Dados do tipo usuario alterado com sucesso!",
                        'class' => "alert-success"
                    ]);
                    return redirect(route('tipoUsuario.index'));
                }elseif ($statusBusca == false){
                    \Session::flash('flash_message', [
                        'msg' => "JÁ EXISTE UM REGISTRO DE TIPO DE USUÁRIO NESSE NOME!",
                        'class' => "alert-danger"
                    ]);
                    return redirect(route('tipoUsuario.index'));
                }
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }
        }else{
            return view('auth.login');
        }

    }
    public function remove($id){

        if (Auth::check()){

            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaAdministrador($idUser);




            if ($admin == true){
                $tipoUsuario = \App\TipoUsuario::find($id);

                $tipoUsuario->delete();

                \Session::flash('flash_message', [
                    'msg' => "Tipo Usuario excluido com sucesso!",
                    'class' => "alert-success"
                ]);
                return redirect()->route('tipoUsuario.index');
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }
        }else{
            return view('auth.login');
        }



    }
}
