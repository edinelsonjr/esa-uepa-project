<?php

namespace App\Http\Controllers;

use App\Ano_Vigencia;
use App\Http\Controllers\Funcoes\FuncoesAuxiliaresController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AnoVigenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            $idUser = Auth::id();
            $verificaAdmin = new FuncoesAuxiliaresController();
            $admin = $verificaAdmin->verificaAdministrador($idUser);
            if ($admin == true){
                $anosVigencias = Ano_Vigencia::all();
                $verificarAnosVigencias = 0;
                if ($anosVigencias == '[]'){
                    return view('ano_vigencia.index', compact('anosVigencias', 'verificarAnosVigencias'));
                }elseif ($anosVigencias !== '[]'){
                    $verificarAnosVigencias = 1;
                    return view('ano_vigencia.index', compact('anosVigencias', 'verificarAnosVigencias') );
                }
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }

        } else {
            return view('auth.login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function desativar($id){
    if (Auth::check()){

        $idUser = Auth::id();

        $verificaAdmin = new FuncoesAuxiliaresController();

        $admin = $verificaAdmin->verificaAdministrador($idUser);

        $idAnoVigencia = $id;


        if ($admin == true){

            DB::table('ano_vigencia')
                ->where('id', $idAnoVigencia)
                ->update(['status_ano_vigencia' => 0]);

            \Session::flash('flash_message', [
                'msg' => "Ano de Vigência desativado com sucesso!",
                'class' => "alert-success"
            ]);
            return redirect()->route('ano_vigencia.detail', $idAnoVigencia);
        }elseif ($admin == false){

            \Session::flash('flash_message', [
                'msg' => "Você não tem permissão pra acessar essa pasta",
                'class' => "alert-danger"
            ]);
            return redirect(route('home'));
        }
    }else{
        return view('auth.login');
    }
}
    public function ativar($id){
        if (Auth::check()){

            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaAdministrador($idUser);

            $idAnoVigencia = $id;

            if ($admin == true){

                DB::table('ano_vigencia')
                    ->where('id', $idAnoVigencia)
                    ->update(['status_ano_vigencia' => 1]);

                \Session::flash('flash_message', [
                    'msg' => "Ano de Vigência Ativado com sucesso!",
                    'class' => "alert-success"
                ]);
                return redirect()->route('ano_vigencia.detail', $idAnoVigencia);
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }
        }else{
            return view('auth.login');
        }
    }

    public function detail($id)
    {
        if (Auth::check()) {
            $idUser = Auth::id();
            $verificaAdmin = new FuncoesAuxiliaresController();

            $usuarioAnoVigecia = DB::table('users')
                ->join('usuario_ano_vigencia', 'users.id', '=', 'usuario_ano_vigencia.usuario_id')
                ->select( 'users.id','users.name', 'usuario_ano_vigencia.id as usuarioAnoVigenciaId')
                ->where('usuario_ano_vigencia.ano_vigencia_id', '=', $id)
                ->where('usuario_ano_vigencia.deleted_at','=', null)
                ->where('usuario_ano_vigencia.status_usuario_ano_vigencia','=', 1)
                ->get();


            $nomeTipoUsuario = DB::table('tipo_user')
                ->select('tipo_user.nome_tipo_usuario')
                ->get();

            $admin = $verificaAdmin->verificaAdministrador($idUser);

            if ($admin == true){
                $anosVigencias = Ano_Vigencia::find($id);


                if ($anosVigencias == null){

                    \Session::flash('flash_message', [
                        'msg' => "Dados Não encontrado.",
                        'class' => "alert-danger"
                    ]);
                    return redirect(route('ano_vigencia.index'));
                }elseif ($anosVigencias !== null){
                    if ($usuarioAnoVigecia == '[]'){
                        $verificarUsuarios = 0;
                        return view('ano_vigencia.detail', compact('anosVigencias', 'verificarAnosVigencias', 'usuarioAnoVigecia', 'nomeTipoUsuario', 'verificarUsuarios'));
                    }elseif ($usuarioAnoVigecia !== '[]'){
                        $verificarUsuarios = 1;
                        return view('ano_vigencia.detail', compact('anosVigencias', 'verificarAnosVigencias', 'usuarioAnoVigecia','nomeTipoUsuario', 'verificarUsuarios'));
                    }
                    $verificarAnosVigencias = 1;
                    return view('ano_vigencia.detail', compact('anosVigencias', 'verificarAnosVigencias') );
                }
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }

        } else {
            return view('auth.login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function fechar($id){

    }

    public function abrir($id){

    }
    public function teste(){
        return view('teste');
    }
    public function save(Request $request){

        if (Auth::check()) {
            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaAdministrador($idUser);


            if ($admin == true){
                $ano_turma = $request['ano_turma'];
                $vigencia = $request['vigencia'];
                $dataInicioVigencia = $request['dataInicioVigencia'];
                $dataFimVigencia = $request['dataFimVigencia'];
                if (Ano_Vigencia::create([
                            'ano' => $ano_turma,
                            'vigencia' => $vigencia,
                            'status_ano_vigencia' => true,
                            'status_ativacao_grupo' => true,
                        ])){
                            \Session::flash('flash_message', [
                                'msg' => "Ano Vigência Cadastrado com sucesso",
                                'class' => "alert-success"
                            ]);                            return redirect(route('ano_vigencia.index'));
                        }else{
                            \Session::flash('flash_message', [
                                'msg' => "Erro no cadastro de usuario",
                                'class' => "alert-danger"
                            ]);
                            return redirect(route('usuario.index'));

                        }
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }

        } else {
            return view('auth.login');
        }

    }
}
