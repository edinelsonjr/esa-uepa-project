<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Funcoes\FuncoesAuxiliaresController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GerenciamentoController extends Controller
{

    public function delete($id){
        if (Auth::check()){

            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaDiscente($idUser);

            $dadosAvaliacao = DB::table('avaliacao')
                ->join('usuario_ano_vigencia', 'usuario_ano_vigencia.id', 'avaliacao.usuario_id')
                ->join('users', 'users.id', 'usuario_ano_vigencia.usuario_id')
                ->select('avaliacao.*')
                ->where('avaliacao.id', '=', $id)
                ->where('users.id', '=', $idUser)
                ->get();


            if ($admin == true){
                if ($dadosAvaliacao == '[]'){
                    \Session::flash('flash_message', [
                        'msg' => "Erro na sua solicitação",
                        'class' => "alert-danger"
                    ]);
                    return redirect()->route('discente');
                }elseif ($dadosAvaliacao !== '[]'){
                    DB::table('avaliacao')
                        ->where('id', $id)
                        ->update(['status_avaliacao' => 0]);

                    DB::table('avaliacao')->where('id', '=', $id)->delete();


                    \Session::flash('flash_message', [
                        'msg' => "Cancelamento de Avaliação realizado com sucesso",
                        'class' => "alert-success"
                    ]);
                    return redirect()->route('discente');
                }

            }elseif ($admin == false){
                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }
        }else{
            return view('auth.login');
        }
    }

    public function perfil(){
        if (Auth::check()) {
            $idUsuario = Auth::id();

            $dados = DB::table('users')
                ->select('users.tipo_usuario_id')
                ->where('users.id', '=', $idUsuario)
                ->value('users.tipo_usuario_id');


            //vai verificar o tipo de perfil do usuario
            if ($dados == 1){//entra na rota do discente
                return redirect(route('discente'));
            }elseif ($dados == 2){

                return redirect(route('administrador'));
            }elseif ($dados == 3){

                return redirect(route('discente'));
            }else{
                \Session::flash('flash_message', [
                    'msg' => "ops, Algo deu errado.",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }
        } else {
            return view('auth.login');
        }
    }
    public function administrador(){

        //
        if (Auth::check()) {
            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaAdministrador($idUser);

            if ($admin == true){
                return view('tela.administrador');
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }

        } else {
            return view('auth.login');
        }


    }


    public function discente(){
        if (Auth::check()) {
            $idUser = Auth::id();



            $verificaDiscente = new FuncoesAuxiliaresController();

            $discente= $verificaDiscente->verificaDiscente($idUser);


            if ($discente == true){

                $queryDadosUser = DB::table('users')
                    ->select(
                        'users.name as nome',
                        'users.matricula as matricula',
                        'users.email as email',
                        'users.status_users as status',
                        'users.tipo_usuario_id as tipoId'
                    )
                    ->where('users.id', '=', $idUser)
                    ->get();


                //logica pra mostrar as avaliações do usuario

                $queryDadosAnoVigencia = DB::table('usuario_ano_vigencia')
                    ->join('ano_vigencia', 'ano_vigencia.id', 'usuario_ano_vigencia.ano_vigencia_id')
                    ->join('avaliacao', 'avaliacao.usuario_id', 'usuario_ano_vigencia.id')
                    ->select(
                        'usuario_ano_vigencia.id as id',
                        'usuario_ano_vigencia.usuario_id as usuario_id',
                        'usuario_ano_vigencia.ano_vigencia_id as ano_vigencia_id',
                        'usuario_ano_vigencia.status_usuario_ano_vigencia as status_ano_vigencia',
                        'ano_vigencia.ano as anoTurma',
                        'ano_vigencia.vigencia as vigenciaAvaliacao',
                        'avaliacao.id as idAvaliacao'
                    )
                    ->where('usuario_ano_vigencia.usuario_id', '=', $idUser)
                    ->where('usuario_ano_vigencia.deleted_at', '=', null)
                    ->where('usuario_ano_vigencia.status_usuario_ano_vigencia', '=', 1)
                    ->where('ano_vigencia.status_ano_vigencia', '=', 0)
                    ->get();




                $queryDadosAnoVigenciaDisponivel = DB::table('usuario_ano_vigencia')
                    ->join('ano_vigencia', 'ano_vigencia.id', 'usuario_ano_vigencia.ano_vigencia_id')
                    ->select(
                        'usuario_ano_vigencia.id as id',
                        'usuario_ano_vigencia.usuario_id as usuario_id',
                        'usuario_ano_vigencia.ano_vigencia_id as ano_vigencia_id',
                        'usuario_ano_vigencia.status_usuario_ano_vigencia as status_ano_vigencia',
                        'ano_vigencia.ano as anoTurma',
                        'ano_vigencia.vigencia as vigenciaAvaliacao'
                    )
                    ->where('usuario_ano_vigencia.usuario_id', '=', $idUser)
                    ->where('usuario_ano_vigencia.deleted_at', '=', null)
                    ->where('usuario_ano_vigencia.status_usuario_ano_vigencia', '=', 1)
                    ->where('ano_vigencia.status_ano_vigencia', '=', 1)
                    ->get();



                $idAnoVIgenciaAvaliacaoDisponivel= DB::table('usuario_ano_vigencia')
                    ->join('ano_vigencia', 'ano_vigencia.id', 'usuario_ano_vigencia.ano_vigencia_id')
                    ->select(
                        'usuario_ano_vigencia.id as idAnoVigencia'
                    )
                    ->where('usuario_ano_vigencia.usuario_id', '=', $idUser)
                    ->where('usuario_ano_vigencia.deleted_at', '=', null)
                    ->where('usuario_ano_vigencia.status_usuario_ano_vigencia', '=', 1)
                    ->where('ano_vigencia.status_ano_vigencia', '=', 1)
                    ->value('ano_vigencia.id');


                ;
                $dadosAvaliacao = DB::table('usuario_ano_vigencia')
                    ->join('avaliacao', 'avaliacao.usuario_id', 'usuario_ano_vigencia.id')
                    ->select(
                        'avaliacao.id'
                    )
                    ->where('avaliacao.usuario_id', '=', $idAnoVIgenciaAvaliacaoDisponivel)
                    ->where('avaliacao.status_avaliacao', '=', true)
                    ->value('avaliacao.id');


                if ($queryDadosAnoVigenciaDisponivel == '[]'){
                    $statusAvaliacaoDisponivel = 0;
                    return view('tela.discente', compact('queryDadosUser', 'queryDadosAnoVigencia', 'queryDadosAnoVigenciaDisponivel', 'statusAvaliacaoDisponivel'));
                }elseif ($queryDadosAnoVigenciaDisponivel !== '[]'){
                    $statusAvaliacaoDisponivel = 1;
                    if ($dadosAvaliacao == '[]'){
                        $dadosAvaliacao = null;
                        return view('tela.discente', compact('queryDadosUser', 'queryDadosAnoVigencia', 'queryDadosAnoVigenciaDisponivel', 'statusAvaliacaoDisponivel', 'dadosAvaliacao'));
                    }elseif ($dadosAvaliacao !== '[]'){
                        return view('tela.discente', compact('queryDadosUser', 'queryDadosAnoVigencia', 'queryDadosAnoVigenciaDisponivel', 'statusAvaliacaoDisponivel', 'dadosAvaliacao'));
                    }

                }

            }elseif ($discente == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }

        } else {
            return view('auth.login');
        }
    }

    public function docente(){
        if (Auth::check()) {
            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaDocente($idUser);

            if ($admin == true){
                return view('tela.docente');
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }

        } else {
            return view('auth.login');
        }
    }

    public function editDiscente(){
        if (Auth::check()) {
            $idUser = Auth::id();

            $verificaDiscente = new FuncoesAuxiliaresController();

            $discente= $verificaDiscente->verificaDiscente($idUser);


            if ($discente == true){

                $buscado = DB::table('users')
                    ->where('id', $idUser)
                    ->get();

                return view('tela.discenteEdit', compact('buscado'));

            }elseif ($discente == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }

        } else {
            return view('auth.login');
        }
    }
}
