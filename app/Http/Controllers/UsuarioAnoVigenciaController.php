<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Funcoes\FuncoesAuxiliaresController;
use App\Usuario_Tem_Ano_Vigencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsuarioAnoVigenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vincularUsuario($id)
    {
        if (Auth::check()){

            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaAdministrador($idUser);

            $idUsuarioAnoVigencia = $id;

            if ($admin == true){
                $dadosAnoVigencia = DB::table('ano_vigencia')
                    ->select('ano_vigencia.*')
                    ->where('ano_vigencia.id', '=', $idUsuarioAnoVigencia)
                    ->where('ano_vigencia.status_ano_vigencia', '=', 1)
                    ->get();

                if ($dadosAnoVigencia == '[]'){
                    \Session::flash('flash_message', [
                        'msg' => "Você não pode vincular usuario ao ano de vigência pois o mesmo está desativado!",
                        'class' => "alert-danger"
                    ]);
                    return redirect()->route('ano_vigencia.detail', $idUsuarioAnoVigencia);
                }elseif ($dadosAnoVigencia !== '[]' ){
                    $first = DB::table('users')
                        ->select('users.id as idUsers', 'users.name as nameUsers')
                        ->where('users.tipo_usuario_id', '=', 1);

                    $users = DB::table('users')
                        ->select('users.id as idUsers', 'users.name as nameUsers')
                        ->where('users.tipo_usuario_id', '=', 3)
                        ->union($first)
                        ->get();
                    return view('ano_vigencia.vincular', compact('idUsuarioAnoVigencia', 'users'));
                }
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }
        }else{
            return view('auth.login');
        }
    }
    public function desvincularUsuario($id)
    {
        if (Auth::check()){

            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaAdministrador($idUser);

            $idUsuarioAnoVigencia = $id;

            $idAnoVIgencia = DB::table('usuario_ano_vigencia')->where('usuario_ano_vigencia.id', $idUsuarioAnoVigencia)->value('ano_vigencia_id');

            if ($admin == true){
                $usuarioAnoVigencia = \App\Usuario_Tem_Ano_Vigencia::find($idUsuarioAnoVigencia);

                $usuarioAnoVigencia->delete();

                \Session::flash('flash_message', [
                    'msg' => "Usuario desvinculado do Ano de Vigência!",
                    'class' => "alert-success"
                ]);
                return redirect()->route('ano_vigencia.detail', $idAnoVIgencia);
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }
        }else{
            return view('auth.login');
        }
    }


    public function  vincularUsuarioSave(Request $request){
        $idAnoVigencia = $request['idAnoVigencia'];
        $idUsuario = $request['usuario'];
        $verificarSeJaEstaVinculado = DB::table('usuario_ano_vigencia')
            ->join('ano_vigencia', 'ano_vigencia.id', 'usuario_ano_vigencia.ano_vigencia_id')
            ->where('usuario_ano_vigencia.usuario_id', '=', $idUsuario)
            ->where('usuario_ano_vigencia.deleted_at', '=', null)
            ->where('usuario_ano_vigencia.status_usuario_ano_vigencia', '=', 1)
            ->where('ano_vigencia.status_ano_vigencia', '=', 1)
            ->get();

        if ($verificarSeJaEstaVinculado == '[]'){
            if (Usuario_Tem_Ano_Vigencia::create([
                'status_usuario_ano_vigencia' => 1,
                'ano_vigencia_id' => $idAnoVigencia,
                'usuario_id' => $request['usuario']
            ])){
                \Session::flash('flash_message', [
                    'msg' => "Usuario adicionado com sucesso",
                    'class' => "alert-success"
                ]);
                return redirect(route('ano_vigencia.detail', $idAnoVigencia));
            }else{
                \Session::flash('flash_message', [
                    'msg' => "Erro no cadastro de usuario",
                    'class' => "alert-danger"
                ]);
                return redirect(route('ano_vigencia.detail', $idAnoVigencia));
            }
        }elseif ($verificarSeJaEstaVinculado !== '[]'){
            \Session::flash('flash_message', [
                'msg' => "Usuario já vinculado a Ano Vigência",
                'class' => "alert-danger"
            ]);
            return redirect(route('ano_vigencia.detail', $idAnoVigencia));
        }



    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
