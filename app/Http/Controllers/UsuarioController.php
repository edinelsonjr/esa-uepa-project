<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Funcoes\FuncoesAuxiliaresController;
use App\TipoUsuario;
use App\User;
use App\Usuario_Tem_Ano_Vigencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::check()) {
            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaAdministrador($idUser);

            if ($admin == true){
                $usuarios = User::all();
                $tipoUsuario = TipoUsuario::all();
                $verificarUsuario = 0;
                if ($usuarios == '[]'){
                    return view('tipo_usuario.index', compact('usuarios', 'verificarUsuario', 'tipoUsuario'));
                }elseif ($usuarios !== '[]'){
                    $verificarUsuario = 1;
                    return view('usuario.index', compact('usuarios', 'verificarUsuario', 'tipoUsuario') );
                }
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }

        } else {
            return view('auth.login');
        }
    }
    public function save(Request $request){

        if (Auth::check()) {
            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaAdministrador($idUser);


            if ($admin == true){
                $nomeUsuario = $request['name'];
                $password = $request['matricula'];
                $email = $request['email'];
                $tipoUsuario = $request['tipo'];
                $palavraMaiuscula = new Funcoes\FuncoesAuxiliaresController();
                $palavraMaiuscula = $palavraMaiuscula->passarMaiuscula($nomeUsuario);
                $statusBuscaNome = new Funcoes\FuncoesAuxiliaresController();
                $nomeTIpo = $palavraMaiuscula;
                $statusBusca = $statusBuscaNome->reconhecerDuplicidade($nomeTIpo, 1);
                $matriculaUsuario = $request['matricula'];
                $verificadorMatricula = new FuncoesAuxiliaresController();
                $verificador = $verificadorMatricula->matricula($matriculaUsuario);

                $verificadorEmail = new FuncoesAuxiliaresController();
                $verificarEmail = $verificadorEmail->email($email);


                    if ($verificador){
                        if ($verificarEmail){
                            if (User::create([
                                'name' => $nomeTIpo,
                                'matricula' => $request['matricula'],
                                'email' => $request['email'],
                                'password' => bcrypt($password),
                                'status_users' => 1,
                                'tipo_usuario_id' => $tipoUsuario
                            ])){
                                \Session::flash('flash_message', [
                                    'msg' => "Usuario adicionado com sucesso",
                                    'class' => "alert-success"
                                ]);
                                return redirect(route('usuario.index'));
                            }else{
                                \Session::flash('flash_message', [
                                    'msg' => "Erro no cadastro de usuario",
                                    'class' => "alert-danger"
                                ]);
                                return redirect(route('usuario.index'));
                            }
                        }elseif ($verificarEmail == false){
                            \Session::flash('flash_message', [
                                'msg' => "Erro no cadastro de usuario email já existente",
                                'class' => "alert-danger"
                            ]);
                            return redirect(route('usuario.index'));
                        }

                    }elseif ($verificador == false){
                        \Session::flash('flash_message', [
                            'msg' => "Erro no cadastro de usuario Matricula já existente",
                            'class' => "alert-danger"
                        ]);
                        return redirect(route('usuario.index'));
                    }
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }

        } else {
            return view('auth.login');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()){
            $idUser = Auth::id();
            $verificaAdmin = new FuncoesAuxiliaresController();
            $admin = $verificaAdmin->verificaAdministrador($idUser);
            if ($admin == true){

                $buscado = DB::table('users')
                    ->where('id', $id)
                    ->get();


                $dadosTipoUsuario = DB::table('tipo_user')
                    ->get();

                return view('usuario.edit', compact('buscado', 'dadosTipoUsuario'));
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }
        }else{
            return view('auth.login');
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        if (Auth::check()){
            $idUser = Auth::id();
            $verificaAdmin = new FuncoesAuxiliaresController();
            $admin = $verificaAdmin->verificaAdministrador($idUser);
            if ($admin == true){
                $nomeUsuario = $request['name'];
                $palavraMaiuscula = new Funcoes\FuncoesAuxiliaresController();
                $palavraMaiuscula = $palavraMaiuscula->passarMaiuscula($nomeUsuario);

                \App\User::find($id)->update($request->all());
                //caso o id não exista
                \Session::flash('flash_message', [
                    'msg' => "Dados do Usuario alterado com sucesso!",
                    'class' => "alert-success"
                ]);
                return redirect(route('usuario.index'));
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }
        }else{
            return view('auth.login');
        }
    }
    public function updateEdit(Request $request, $id){
        if (Auth::check()){
            $idUser = Auth::id();
            $verificaAdmin = new FuncoesAuxiliaresController();
            $admin = $verificaAdmin->verificaDiscente($idUser);
            if ($admin == true){
                $nomeUsuario = $request['name'];
                $palavraMaiuscula = new Funcoes\FuncoesAuxiliaresController();
                $palavraMaiuscula = $palavraMaiuscula->passarMaiuscula($nomeUsuario);

                \App\User::find($id)->update($request->all());
                //caso o id não exista
                \Session::flash('flash_message', [
                    'msg' => "Dados do Usuario alterado com sucesso!",
                    'class' => "alert-success"
                ]);
                return redirect(route('discente'));
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }
        }else{
            return view('auth.login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove($id)
    {
        if (Auth::check()){

            $idUser = Auth::id();

            $verificaAdmin = new FuncoesAuxiliaresController();

            $admin = $verificaAdmin->verificaAdministrador($idUser);


            if ($admin == true){
                $user = \App\User::find($id);

                $user->delete();

                \Session::flash('flash_message', [
                    'msg' => "Usuario excluido com sucesso!",
                    'class' => "alert-success"
                ]);
                return redirect()->route('usuario.index');
            }elseif ($admin == false){

                \Session::flash('flash_message', [
                    'msg' => "Você não tem permissão pra acessar essa pasta",
                    'class' => "alert-danger"
                ]);
                return redirect(route('home'));
            }
        }else{
            return view('auth.login');
        }
    }

    public function cadastroTeste(){
        return view('usuario.cadastroTeste');
    }

    public function saveCadastroTeste(Request $request)
    {


        $nomeUsuario = $request['name'];
        $password = $request['password'];
        $email = $request['email'];
        $tipoUsuario = $request['tipo'];
        $palavraMaiuscula = new Funcoes\FuncoesAuxiliaresController();
        $palavraMaiuscula = $palavraMaiuscula->passarMaiuscula($nomeUsuario);
        $statusBuscaNome = new Funcoes\FuncoesAuxiliaresController();
        $nomeTIpo = $palavraMaiuscula;
        $statusBusca = $statusBuscaNome->reconhecerDuplicidade($nomeTIpo, 1);
        $matriculaUsuario = $request['matricula'];
        $verificadorMatricula = new FuncoesAuxiliaresController();
        $verificador = $verificadorMatricula->matricula($matriculaUsuario);

        $verificadorEmail = new FuncoesAuxiliaresController();
        $verificarEmail = $verificadorEmail->email($email);


        if ($verificador) {
            if ($verificarEmail) {
                $usuario = User::create([
                    'name' => $nomeTIpo,
                    'matricula' => $request['matricula'],
                    'email' => $request['email'],
                    'password' => bcrypt($password),
                    'status_users' => 1,
                    'tipo_usuario_id' => $tipoUsuario
                ]);
                $idusuario = $usuario->id;


                \Session::flash('flash_message', [
                    'msg' => "Usuario adicionado com sucesso",
                    'class' => "alert-success"
                ]);
                return redirect(route('vincular.cadastroTeste', $idusuario));
            } elseif ($verificarEmail == false) {
                \Session::flash('flash_message', [
                    'msg' => "Erro no cadastro de usuario email já existente",
                    'class' => "alert-danger"
                ]);
                return redirect(route('usuario.index'));
            }

        } elseif ($verificador == false) {
            \Session::flash('flash_message', [
                'msg' => "Erro no cadastro de usuario Matricula já existente",
                'class' => "alert-danger"
            ]);
            return redirect(route('usuario.index'));
        }

    }

    public function vincularCadastroTeste($id){
        Usuario_Tem_Ano_Vigencia::create([
            'status_usuario_ano_vigencia' => 1,
            'ano_vigencia_id' => 1,
            'usuario_id' => $id
        ]);
        return redirect(route('discente'));
    }
}
