<?php

namespace App\Http\Controllers\Funcoes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Return_;

class FuncoesAuxiliaresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    function reconhecerDuplicidade($palavra, $tabela){
        $valorTabela = $tabela;
        $palavraRecebida = $palavra;
        if($valorTabela == 1){//vai entrar na verificação do usuario
            $buscado = DB::table('users')
                ->where('name', $palavraRecebida)
                ->where('deleted_at', null)
                ->get();
            if($buscado == '[]'){
                return true;
            }else{
                return false;
            }
        }elseif ($valorTabela == 2){//vai entrar na verificação do tipo usuario

            $buscado = DB::table('tipo_user')
                ->where('nome_tipo_usuario', $palavraRecebida)
                ->where('deleted_at', null)
                ->get();
            if($buscado == '[]'){
                return true;
            }else{
                return false;
            }
        }



    }

    public function verificaTipoPerfil(){



    }


    function passarMaiuscula($palavra){
        $palavraMaiuscula = $palavra;
        $palavraMaiuscula = strtoupper($palavraMaiuscula);
        return $palavraMaiuscula;
    }

    public function matricula($matricula){

        $matriculaUsuario = DB::table('users')
            ->where('matricula', $matricula)
            ->get();
        if($matriculaUsuario == '[]'){
            return true;
        }else{
            return false;
        }
    }

    public function verificaAdministrador($idUser){

        $idUsuario = $idUser;

        $dadosAdmin = DB::table('users')
            ->join('tipo_user', 'users.tipo_usuario_id', '=', 'tipo_user.id')
            ->select('users.*', 'tipo_user.nome_tipo_usuario')
            ->where('users.id', '=', $idUsuario)
            ->where('tipo_user.id', '=', 2)
            ->get();



        if ($dadosAdmin == '[]'){
            return false;
        }elseif ($dadosAdmin !==  '[]'){
            return true;
        }
    }
    public function verificaDiscente($idUser){

        $idUsuario = $idUser;

        $dadosAdmin1 = DB::table('users')
            ->join('tipo_user', 'users.tipo_usuario_id', '=', 'tipo_user.id')
            ->select('users.*', 'tipo_user.nome_tipo_usuario')
            ->where('users.id', '=', $idUsuario)
            ->where('tipo_user.id', '=', 1);

        $dadosAdmin = DB::table('users')
            ->join('tipo_user', 'users.tipo_usuario_id', '=', 'tipo_user.id')
            ->select('users.*', 'tipo_user.nome_tipo_usuario')
            ->where('users.id', '=', $idUsuario)
            ->where('tipo_user.id', '=', 3)
            ->union($dadosAdmin1)
            ->get();

        if ($dadosAdmin == null){
            return "Entrou no false";
        }elseif ($dadosAdmin !==  null){
            return "Entrou no true";
            return true;
        }
    }

    public function verificaDocente($idUser){
        $idUsuario = $idUser;

        $dadosAdmin = DB::table('users')
            ->join('tipo_user', 'users.tipo_usuario_id', '=', 'tipo_user.id')
            ->select('users.*', 'tipo_user.nome_tipo_usuario')
            ->where('users.id', '=', $idUsuario)
            ->where('tipo_user.id', '=', 3)
            ->get();
        if ($dadosAdmin == '[]'){
            return false;
        }elseif ($dadosAdmin !==  '[]'){
            return true;
        }
    }
    public function email($email){

        $emailUsuario = DB::table('users')
            ->where('email', $email)
            ->get();
        if($emailUsuario == '[]'){
            return true;
        }else{
            return false;
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
