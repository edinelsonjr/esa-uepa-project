<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Usuario_Tem_Ano_Vigencia extends Model
{
    protected $table = 'usuario_ano_vigencia';
    use SoftDeletes;
    protected $fillable =
        [
            'status_usuario_ano_vigencia',
            'usuario_id',
            'ano_vigencia_id',
        ];

    /****************************************************************************
     * RELACIONAMENTO ENTRE AS TABELAS
     ****************************************************************************/

    public function usuarioTemAnoVigenciaTemVigencia()
    {
        return $this->hasMany('App\User');
    }

    public function usuarioTemAnoVigenciaEstaContidoNoAnoVigencia()
    {
        return $this->belongsTo('App\Ano_Vigencia');
    }

    public function Avaliacao()
    {
        return $this->belongsTo('App\Avaliacao');
    }
}
