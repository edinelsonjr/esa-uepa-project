<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoUsuario extends Model
{
    use SoftDeletes ;
    protected $table = 'tipo_user';

    protected $fillable =
        [
            'nome_tipo_usuario',
            'status_tipo_usuario'
        ];

    /****************************************************************************
     * RELACIONAMENTO ENTRE AS TABELAS
     ****************************************************************************/

    public function tipo_usuario_tem_usuario()
    {
        return $this->hasMany('App\User');
    }
}
