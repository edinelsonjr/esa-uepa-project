<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Avaliacao extends Model
{
    use SoftDeletes ;
    protected $table = 'avaliacao';

    protected $fillable =
        [
            'status_avaliacao',
            'resp_perg1',
            'resp_perg2',
            'resp_perg3',
            'resp_perg4',
            'resp_perg5',
            'resp_perg6',
            'resp_perg7',
            'resp_perg8',
            'resp_perg9',
            'resp_perg10',
            'observacao',
            'usuario_id',
            'status_avaliacao',
        ];

    /****************************************************************************
     * RELACIONAMENTO ENTRE AS TABELAS
     ****************************************************************************/

    public function avaliacaoTemUsuarioAnoVigencia()
    {
        return $this->hasMany('App\Usuario_Tem_Ano_Vigencia');
    }
}
