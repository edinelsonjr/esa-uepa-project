<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvaliacaoNovaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacao', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status_avaliacao');
            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('users');
            $table->integer('resp_perg1');
            $table->integer('resp_perg2');
            $table->integer('resp_perg3');
            $table->integer('resp_perg4');
            $table->integer('resp_perg5');
            $table->integer('resp_perg6');
            $table->integer('resp_perg7');
            $table->integer('resp_perg8');
            $table->integer('resp_perg9');
            $table->integer('resp_perg10');
            $table->string('observacao');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaliacao');
    }
}
