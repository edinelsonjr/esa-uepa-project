<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnoVigenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ano_vigencia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ano');
            $table->string('vigencia');
            $table->boolean('status_ativacao_grupo');
            $table->boolean('status_ano_vigencia');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ano_vigencia');
    }
}
