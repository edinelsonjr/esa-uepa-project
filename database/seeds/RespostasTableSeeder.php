<?php

use Illuminate\Database\Seeder;

class RespostasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('respostas_avaliacao')->insert([
            'resposta' => 'Concordo totalmente',
            'status_resposta' => 1,
        ]);
        DB::table('respostas_avaliacao')->insert([
            'resposta' => 'Concordo parcialmente',
            'status_resposta' => 1,
        ]);
        DB::table('respostas_avaliacao')->insert([
            'resposta' => 'Não sei ',
            'status_resposta' => 1,
        ]);
        DB::table('respostas_avaliacao')->insert([
            'resposta' => 'Discordo parcialmente',
            'status_resposta' => 1,
        ]);
        DB::table('respostas_avaliacao')->insert([
            'resposta' => 'Concordo totalmente',
            'status_resposta' => 1,
        ]);
        DB::table('respostas_avaliacao')->insert([
            'resposta' => 'Discordo totalmente',
            'status_resposta' => 1,
        ]);
    }
}
