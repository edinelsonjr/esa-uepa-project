<?php

use Illuminate\Database\Seeder;

class AnoVigenciaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ano_vigencia')->insert([
            'ano' => 2014,
            'vigencia' => 2018,
            'status_ativacao_grupo' => 1,
            'status_ano_vigencia' => 1,
        ]);
    }
}
