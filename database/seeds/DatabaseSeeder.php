<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $this->call('RespostasTableSeeder');
        $this->call('TipoUsuarioTableSeeder');
        $this->call('UsuarioTableSeeder');
        $this->call('AnoVigenciaTableSeeder');

    }
}

class RespostasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('respostas_avaliacao')->insert([
            'resposta' => 'Concordo totalmente',
            'status_resposta' => 1,
        ]);
        DB::table('respostas_avaliacao')->insert([
            'resposta' => 'Concordo parcialmente',
            'status_resposta' => 1,
        ]);
        DB::table('respostas_avaliacao')->insert([
            'resposta' => 'Não sei ',
            'status_resposta' => 1,
        ]);
        DB::table('respostas_avaliacao')->insert([
            'resposta' => 'Discordo parcialmente',
            'status_resposta' => 1,
        ]);
        DB::table('respostas_avaliacao')->insert([
            'resposta' => 'Concordo totalmente',
            'status_resposta' => 1,
        ]);
        DB::table('respostas_avaliacao')->insert([
            'resposta' => 'Discordo totalmente',
            'status_resposta' => 1,
        ]);
    }
}
class TipoUsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_user')->insert([
            'nome_tipo_usuario' => 'DISCENTE',
            'status_tipo_usuario' => 1,
        ]);
        DB::table('tipo_user')->insert([
            'nome_tipo_usuario' => 'ADMINISTRADOR',
            'status_tipo_usuario' => 1,
        ]);
        DB::table('tipo_user')->insert([
            'nome_tipo_usuario' => 'DOCENTE',
            'status_tipo_usuario' => 1,
        ]);

    }
}
class AnoVigenciaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ano_vigencia')->insert([
            'ano' => 2014,
            'vigencia' => 2018,
            'status_ativacao_grupo' => 1,
            'status_ano_vigencia' => 1,
        ]);
    }
}

class UsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'ADMINISTRADOR',
            'email' => 'administrador'.'@gmail.com',
            'matricula' => '78945613',
            'tipo_usuario_id' => 2,
            'status_users' => 1,
            'password' => bcrypt('1234567'),
        ]);

        DB::table('users')->insert([
            'name' => 'DISCENTE',
            'email' => 'discente'.'@gmail.com',
            'matricula' => 1234567,
            'tipo_usuario_id' => 1,
            'status_users' => 1,
            'password' => bcrypt('1234567'),
        ]);

        DB::table('users')->insert([
            'name' => 'DOCENTE',
            'email' => 'docente'.'@gmail.com',
            'matricula' => 78944561227,
            'tipo_usuario_id' => 3,
            'status_users' => 1,
            'password' => bcrypt('1234567'),
        ]);
    }
}