<?php

use Illuminate\Database\Seeder;

class UsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => str_random(10),
            'email' => 'administrador'.'@gmail.com',
            'matricula' => 'administrador'.'@gmail.com',
            'tipo_usuario_id' => 2,
            'atatus_user' => 1,
            'password' => bcrypt('1234567'),
        ]);
    }
}
