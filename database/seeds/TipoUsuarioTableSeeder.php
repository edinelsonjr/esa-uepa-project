<?php

use Illuminate\Database\Seeder;

class TipoUsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_user')->insert([
            'nome_tipo_usuario' => 'DISCENTE',
            'status_tipo_usuario' => 1,
        ]);
        DB::table('tipo_user')->insert([
            'nome_tipo_usuario' => 'ADMINISTRADOR',
            'status_tipo_usuario' => 1,
        ]);
        DB::table('tipo_user')->insert([
            'nome_tipo_usuario' => 'DOCENTE',
            'status_tipo_usuario' => 1,
        ]);

    }
}
